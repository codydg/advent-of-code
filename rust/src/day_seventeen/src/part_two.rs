use std::env;
use std::fs;
use regex::Regex;
use regex::Captures;
use lazy_static::lazy_static;

struct Target {
    x_min : i32,
    x_max : i32,
    y_min : i32,
    y_max : i32
}

impl Target {
    fn new(cap : Captures) -> Target {
        Target{x_min: get_num(&cap[1]), x_max: get_num(&cap[2]), y_min: get_num(&cap[3]), y_max: get_num(&cap[4])}
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    lazy_static! {
        static ref LINE_REGEX: Regex = Regex::new("target area: x=([-0-9]+)\\.\\.([-0-9]+),\\s*y=([-0-9]+)\\.\\.([-0-9]+)").unwrap();
    }

    contents.trim_end()
        .split("\n")
        .map(|x| x.trim_end())
        .map(|x| LINE_REGEX.captures_iter(x))
        .flat_map(|x| x)
        .map(|x| Target::new(x))
        .for_each(|target| {
            let mut distinct_velocities = 0;
            for y_vel in target.y_min..-target.y_min {
                for x_vel in 0..=target.x_max {
                    if simulate(x_vel, y_vel, &target) {
                        distinct_velocities += 1;
                    }
                }
            }
            println!("{}", distinct_velocities);
        });
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn simulate(mut x_vel : i32, mut y_vel : i32, target : &Target) -> bool {
    let mut x = 0;
    let mut y = 0;
    while y > target.y_min {
        x += x_vel;
        y += y_vel;
        if x_vel > 0 {
            x_vel -= 1;
        }
        y_vel -= 1;
        if (target.x_min <= x) && (target.x_max >= x) && (target.y_min <= y) && (target.y_max >= y) {
            return true;
        }
    }
    return false;
}

fn get_num(num_str: &str) -> i32 {
    match num_str.parse::<i32>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}
