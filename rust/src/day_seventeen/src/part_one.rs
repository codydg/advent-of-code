use std::env;
use std::fs;
use regex::Regex;
use regex::Captures;
use lazy_static::lazy_static;

struct Target {
    x_min : i32,
    x_max : i32,
    y_min : i32,
    y_max : i32
}

impl Target {
    fn new(cap : Captures) -> Target {
        Target{x_min: get_num(&cap[1]), x_max: get_num(&cap[2]), y_min: get_num(&cap[3]), y_max: get_num(&cap[4])}
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    lazy_static! {
        static ref LINE_REGEX: Regex = Regex::new("target area: x=([-0-9]+)\\.\\.([-0-9]+),\\s*y=([-0-9]+)\\.\\.([-0-9]+)").unwrap();
    }

    contents.trim_end()
        .split("\n")
        .map(|x| x.trim_end())
        .map(|x| LINE_REGEX.captures_iter(x))
        .flat_map(|x| x)
        .map(|x| Target::new(x))
        .for_each(|target| {
            let n = -target.y_min - 1;
            println!("{}", n * (n+1) / 2);
        });
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_num(num_str: &str) -> i32 {
    match num_str.parse::<i32>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}
