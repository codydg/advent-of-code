use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let num_strs = contents.trim_end().split(",").collect::<Vec<_>>();
    // Convert num strings to nums
    let mut fish : Vec<u8> = Vec::new();
    for num_str in num_strs {
        fish.push(get_num(&num_str));
    }
    // Simulate 80 days' growth
    for _ in 0..80 {
        let mut count = 0;
        for num in fish.iter_mut() {
            *num = match *num {
                0 => {
                    count += 1;
                    6
                },
                other => other - 1
            }
        }
        // Add lanternfish
        for _ in 0..count {
            fish.push(8);
        }
    }
    println!("{}", fish.len());
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_num(num_str: &str) -> u8 {
    match num_str.parse::<u8>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}
