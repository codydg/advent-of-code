use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);
    let days = 256;

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let num_strs = contents.trim_end().split(",").collect::<Vec<_>>();
    // Convert num strings to nums
    let mut fish = [0usize; 9];
    for num_str in num_strs {
        fish[get_num(&num_str)] += 1;
    }
    // Simulate growth
    for _ in 0..days {
        let rollover = fish[0];
        for idx in 0..8 {
            fish[idx] = fish[idx + 1];
        }
        fish[8] = rollover;
        fish[6] += rollover;
    }
    println!("{}", fish.iter().fold(0, |a, &b| a + b));
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_num(num_str: &str) -> usize {
    match num_str.parse::<usize>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}
