use std::env;
use std::fs;
use std::cmp;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let mut crab_positions = contents.trim_end().split(",").map(|x| get_num(x)).collect::<Vec<_>>();
    crab_positions.sort();

    let mut min_fuel = i32::MAX;
    for i in crab_positions[0]..=crab_positions[crab_positions.len()-1] {
        let fuel = crab_positions.iter().map(|x| (x - i).abs()).fold(0, |a, b| (b + 1) * b / 2 + a);
        min_fuel = cmp::min(min_fuel, fuel);
    }
    println!("{}", min_fuel);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_num(num_str: &str) -> i32 {
    match num_str.parse::<i32>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}
