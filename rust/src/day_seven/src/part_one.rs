use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let mut crab_positions = contents.trim_end().split(",").map(|x| get_num(x)).collect::<Vec<_>>();
    crab_positions.sort();

    let median = crab_positions[(crab_positions.len() / 2) as usize];
    let fuel = crab_positions.iter().fold(0, |a, &b| (b - median).abs() + a);
    println!("{}", fuel);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_num(num_str: &str) -> i32 {
    match num_str.parse::<i32>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}
