use std::env;
use std::fs;
use regex::Regex;
use lazy_static::lazy_static;
use std::collections::HashMap;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    lazy_static! {
        static ref INSERTION_REGEX: Regex = Regex::new("([A-Z])([A-Z]) -> ([A-Z])").unwrap();
    }

    let mut lines_iter = contents.trim_end().split("\n").map(|x| x.trim_end());
    let mut polymer : Vec<char> = lines_iter.next().unwrap().chars().collect();

    let mut insertions : HashMap<char, HashMap<char, char>> = HashMap::new();

    lines_iter.map(|line| INSERTION_REGEX.captures_iter(line).collect::<Vec<_>>())
        .filter(|caps| caps.len() == 1)
        .map(|caps| [to_char(&caps[0][1]), to_char(&caps[0][2]), to_char(&caps[0][3])])
        .for_each(|caps| {
            match insertions.get_mut(&caps[0]) {
                None => {
                    let mut to_insert : HashMap<char, char> = HashMap::new();
                    to_insert.insert(caps[1], caps[2]);
                    insertions.insert(caps[0], to_insert);
                },
                Some(map) => { map.insert(caps[1], caps[2]); }
            }
        });

    for _ in 1..=10 {
        let mut poly_copy : Vec<char> = Vec::new();
        let mut poly_iter = polymer.iter();
        let mut last_letter = poly_iter.next().unwrap();
        poly_copy.push(*last_letter);

        for letter in poly_iter {
            match insertions.get(last_letter) {
                None => (),
                Some(map) => {
                    match map.get(letter) {
                        None => (),
                        Some(to_push) => {
                            poly_copy.push(*to_push);
                        }
                    }
                }
            };

            poly_copy.push(*letter);
            last_letter = letter;
        }
        polymer = poly_copy;
    }

    let mut counts : HashMap<char, usize> = HashMap::new();
    for poly_char in polymer {
        match counts.get_mut(&poly_char) {
            Some(val) => *val += 1,
            None => { counts.insert(poly_char, 1); }
        };
    }
    println!("{}", counts.values().max().unwrap() - counts.values().min().unwrap());
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn to_char(string : &str) -> char {
    string.chars().next().unwrap()
}
