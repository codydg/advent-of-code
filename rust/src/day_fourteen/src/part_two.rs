use std::env;
use std::fs;
use regex::Regex;
use lazy_static::lazy_static;
use std::collections::HashMap;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    lazy_static! {
        static ref INSERTION_REGEX: Regex = Regex::new("([A-Z])([A-Z]) -> ([A-Z])").unwrap();
    }

    let mut lines_iter = contents.trim_end().split("\n").map(|x| x.trim_end());

    let mut polymer : HashMap<String, usize> = HashMap::new();
    let mut start_poly_iter = lines_iter.next().unwrap().chars();
    let mut last_char = start_poly_iter.next().unwrap();
    start_poly_iter.for_each(|letter| {
        let pair : String = format!("{}{}", last_char, letter);
        match polymer.get_mut(&pair) {
            Some(val) => *val += 1,
            None => { polymer.insert(pair, 1); }
        };
        last_char = letter;
    });

    let insertions : HashMap<String, (String, String)> = lines_iter.map(|line| INSERTION_REGEX.captures_iter(line).collect::<Vec<_>>())
        .filter(|caps| caps.len() == 1)
        .map(|caps| (to_char(&caps[0][1]), to_char(&caps[0][2]), to_char(&caps[0][3])))
        .map(|letters| (format!("{}{}", letters.0, letters.1), (format!("{}{}", letters.0, letters.2), format!("{}{}", letters.2, letters.1))))
        .collect();

    for _ in 1..=40 {
        let mut new_polymer = polymer.clone();
        for (letters, count) in &polymer {
            if *count == 0 {
                continue;
            }
            if let Some(insertion) = insertions.get(letters) {
                *new_polymer.get_mut(letters).unwrap() -= count;
                match new_polymer.get_mut(&insertion.0) {
                    Some(to_increment) => *to_increment += count,
                    None => { new_polymer.insert(insertion.0.clone(), *count); }
                }
                match new_polymer.get_mut(&insertion.1) {
                    Some(to_increment) => *to_increment += count,
                    None => { new_polymer.insert(insertion.1.clone(), *count); }
                }
            }
        }
        polymer = new_polymer;
    }

    let mut counts : HashMap<char, usize> = HashMap::new();
    for (letters, count) in polymer {
        for letter in letters.chars() {
            match counts.get_mut(&letter) {
                Some(val) => *val += count,
                None => { counts.insert(letter, count); }
            };
        }
    }
    println!("{}", actual_count(*counts.values().max().unwrap()) - actual_count(*counts.values().min().unwrap()));
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn to_char(string : &str) -> char {
    let mut chars = string.chars();
    let ret_val = chars.next().unwrap();
    assert_eq!(chars.count(), 0);
    ret_val
}

fn actual_count(count : usize) -> usize {
    (count + 1) / 2
}
