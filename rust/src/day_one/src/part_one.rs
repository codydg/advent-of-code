use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let mut last_num = std::u32::MAX;
    let mut count = 0;
    for line in contents.split("\n") {
        let num = match line.trim_end().parse::<u32>() {
            Ok(num) => num,
            Err(error) => match error.kind() {
                std::num::IntErrorKind::Empty => 0,
                other => {
                    panic!("Test {:?}", other);
                }
            }
        };
        if num > last_num {
            count += 1;
        }
        last_num = num;
    }

    println!("{}", count);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
