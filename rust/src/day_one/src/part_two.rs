use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let mut last_num = std::u32::MAX;
    let mut count = 0;
    let mut i = 0;
    let mut buffer: [u32; 3] = [0; 3];
    for line in contents.split("\n") {
        buffer[i % 3] = match line.trim_end().parse::<u32>() {
            Ok(num) => num,
            Err(error) => match error.kind() {
                std::num::IntErrorKind::Empty => 0,
                other => {
                    panic!("Test {:?}", other);
                }
            }
        };
        if i >= 2 {
            let num = buffer[0] + buffer[1] + buffer[2];
            if num > last_num {
                count += 1;
            }
            last_num = num;
        }
        i += 1;
    }

    println!("{}", count);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
