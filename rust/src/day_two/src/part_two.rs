use std::env;
use std::fs;
use lazy_static::lazy_static;
use regex::Regex;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let mut x = 0;
    let mut y = 0;
    let mut aim = 0;
    lazy_static! {
        static ref INPUT_REGEX: Regex = Regex::new("^([a-zA-Z]*)\\s*([0-9]*)$").unwrap();
    }
    for result in contents.split("\n").map(|x| INPUT_REGEX.captures_iter(x.trim_end())) {
        for cap in result {
            match cap[2].parse::<u32>() {
                Ok(num) => {
                    match &cap[1] {
                        "forward" => {
                            x += num;
                            y += aim * num;
                        },
                        "up" => aim -= num,
                        "down" => aim += num,
                        _ => {
                            panic!("Unexpected string in file: {}", &cap[1]);
                        }
                    }
                },
                Err(error) => match error.kind() {
                    std::num::IntErrorKind::Empty => (),
                    other => {
                        panic!("Test {:?}", other);
                    }
                }
            };
        }
    }

    println!("{}", x * y);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
