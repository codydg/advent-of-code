use std::env;
use std::fs;
use lazy_static::lazy_static;
use regex::Regex;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let mut x = 0;
    let mut y = 0;
    for line in contents.split("\n") {
        let short_line = line.trim_end();

        lazy_static! {
            static ref INPUT_REGEX: Regex = Regex::new("^([a-zA-Z]*)\\s*([0-9]*)$").unwrap();
        }

        let result = INPUT_REGEX.captures_iter(short_line);
        for cap in result {
            let num = match cap[2].parse::<u32>() {
                Ok(num) => num,
                Err(error) => match error.kind() {
                    std::num::IntErrorKind::Empty => 0,
                    other => {
                        panic!("Test {:?}", other);
                    }
                }
            };
            if num != 0 {
                match &cap[1] {
                    "forward" => x += num,
                    "up" => y -= num,
                    "down" => y += num,
                    _ => {
                        panic!("Unexpected string in file: {}", &cap[1]);
                    }
                }
            }
        }
    }

    println!("{}", x * y);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
