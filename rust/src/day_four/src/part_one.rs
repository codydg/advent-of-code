use std::env;
use std::fs;
use regex::Regex;
use lazy_static::lazy_static;

type Board = [[(u8, bool); 5]; 5];
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let lines = contents.split("\n").collect::<Vec<_>>();
    let mut numbers: Vec<u8> = Vec::new();
    let mut boards: Vec<Board> = Vec::new();
    let mut first = true;
    let mut i = 0usize;
    for line in lines {
        let short_line = line.trim_end();
        if first {
            for num_str in short_line.split(",") {
                numbers.push(get_num(&num_str));
            }
            first = false;
        } else if short_line.len() != 0 {
            lazy_static! {
                static ref INPUT_REGEX: Regex = Regex::new("\\d+").unwrap();
            }
            let result = INPUT_REGEX.captures_iter(short_line);
            for cap in result {
                let num = get_num(&cap[0]);
                let board_idx = (i / 25) as usize;
                let i_board = i - (board_idx * 25) as usize;
                let col_idx = i_board / 5;
                let row_idx = i_board % 5;
                if board_idx >= boards.len() {
                    boards.push([[(0, false); 5]; 5]);
                }
                boards[board_idx][col_idx][row_idx].0 = num;
                i += 1;
            }
        }
    }
    assert_eq!(i % 25, 0);

    let winning_score = check_boards(numbers, boards);
    assert_ne!(winning_score, usize::MAX);
    println!("{}", winning_score);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_num(num_str : &str) -> u8 {
    match num_str.parse::<u8>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}

fn check_boards(numbers : Vec<u8>, mut boards : Vec<Board>) -> usize {
    for num in numbers {
        for board in boards.iter_mut() {
            let mut to_check = (usize::MAX, usize::MAX);
            for (col_idx, col) in board.iter_mut().enumerate() {
                for (row_idx, row) in col.iter_mut().enumerate() {
                    if row.0 == num {
                        row.1 = true;
                        to_check = (col_idx, row_idx);
                    }
                }
            }
            if to_check.0 != usize::MAX {
                let mut col_bingo = true;
                let mut row_bingo = true;
                for i in 0..5 {
                    col_bingo &= board[to_check.0][i].1;
                    row_bingo &= board[i][to_check.1].1;
                }
                if col_bingo || row_bingo {
                    return num as usize * score_board(&board);
                }
            }
        }
    }
    usize::MAX
}

fn score_board(board : &Board) -> usize {
    let mut score = 0usize;
    for col in board {
        for row in col {
            if !row.1 {
                score += row.0 as usize;
            }
        }
    }
    score
}
