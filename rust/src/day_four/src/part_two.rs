use std::env;
use std::fs;
use regex::Regex;
use lazy_static::lazy_static;

type Board = ([[(u8, bool); 5]; 5], bool);
fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let lines = contents.split("\n").map(|x| x.trim_end()).collect::<Vec<_>>();
    let numbers: Vec<u8> = lines[0].split(",").map(|x| get_num(&x)).collect::<Vec<_>>();
    let mut boards: Vec<Board> = Vec::new();
    let mut i = 0usize;
    for line in lines.iter().skip(1).filter(|x| x.len() != 0) {
        lazy_static! {
            static ref INPUT_REGEX: Regex = Regex::new("\\d+").unwrap();
        }
        for num in INPUT_REGEX.captures_iter(line).map(|x| get_num(&x[0])) {
            let board_idx = (i / 25) as usize;
            let i_board = i - (board_idx * 25) as usize;
            let col_idx = i_board / 5;
            let row_idx = i_board % 5;
            if board_idx >= boards.len() {
                boards.push(([[(0, false); 5]; 5], false));
            }
            boards[board_idx].0[col_idx][row_idx].0 = num;
            i += 1;
        }
    }
    assert_eq!(i % 25, 0);

    let winning_score = check_boards(numbers, boards);
    assert_ne!(winning_score, usize::MAX);
    println!("{}", winning_score);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_num(num_str : &str) -> u8 {
    match num_str.parse::<u8>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}

fn check_boards(numbers : Vec<u8>, mut boards : Vec<Board>) -> usize {
    let mut boards_left = boards.len();
    for num in numbers {
        for board in boards.iter_mut() {
            if board.1 {
                continue;
            }
            let mut to_check = (usize::MAX, usize::MAX);
            for (col_idx, col) in board.0.iter_mut().enumerate() {
                for (row_idx, row) in col.iter_mut().enumerate() {
                    if row.0 == num {
                        row.1 = true;
                        to_check = (col_idx, row_idx);
                    }
                }
            }
            if to_check.0 != usize::MAX {
                let mut col_bingo = true;
                let mut row_bingo = true;
                for i in 0..5 {
                    col_bingo &= board.0[to_check.0][i].1;
                    row_bingo &= board.0[i][to_check.1].1;
                }
                if col_bingo || row_bingo {
                    board.1 = true;
                    boards_left -= 1;
                    if boards_left == 0 {
                        return num as usize * score_board(&board);
                    }
                }
            }
        }
    }
    usize::MAX
}

fn score_board(board : &Board) -> usize {
    let mut score = 0usize;
    for col in board.0 {
        for row in col {
            if !row.1 {
                score += row.0 as usize;
            }
        }
    }
    score
}
