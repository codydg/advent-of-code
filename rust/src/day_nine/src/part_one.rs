use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let lines : Vec<Vec<i8>> = contents.trim_end().split("\n").map(|x| x.trim_end().chars().map(|x| x.to_digit(10).unwrap() as i8).collect()).collect::<Vec<_>>();
    let width = lines[0].len() as i8;
    let height = lines.len() as i8;

    let mut risk = 0usize;
    for y in 0..height {
        for x in 0..width {
            let val = lines[y as usize][x as usize];
            if vec!{(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)}.iter()
               .filter(|&&coord| coord.0 >= 0 && coord.0 < width && coord.1 >= 0 && coord.1 < height)
               .all(|&x| lines[x.1 as usize][x.0 as usize] > val) {
                risk += val as usize + 1;
            }
        }
    }
    println!("{}", risk);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
