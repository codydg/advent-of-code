use std::env;
use std::fs;

#[derive(Clone, PartialEq)]
struct Coord {
    x: i32,
    y: i32,
}

impl Coord {
    fn new(x : i32, y:i32) -> Coord {
        Coord{x:x, y:y}
    }

    fn get_neighbors(&self, width : i32, height : i32) -> Vec<Coord> {
        let mut res : Vec<Coord> = vec!{Coord::new(self.x + 1, self.y), Coord::new(self.x - 1, self.y), Coord::new(self.x, self.y + 1), Coord::new(self.x, self.y - 1)};
        res.retain(|coord| coord.x >= 0 && coord.x < width && coord.y >= 0 && coord.y < height);
        res
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let rows : Vec<Vec<i8>> = contents.trim_end().split("\n").map(|x| x.trim_end().chars().map(|x| x.to_digit(10).unwrap() as i8).collect()).collect::<Vec<_>>();
    let width = rows[0].len() as i32;
    let height = rows.len() as i32;
    let mut basins : Vec<usize> = Vec::new();
    for y in 0..height {
        for x in 0..width {
            let coord = Coord::new(x, y);
            let val = rows[y as usize][x as usize];
            if coord.get_neighbors(width, height).iter().all(|coord| rows[coord.y as usize][coord.x as usize] > val) {
                let mut basin : Vec<Coord> = vec!(coord);
                let mut last_len = 0usize;
                while basin.len() > last_len {
                    last_len = basin.len();
                    for i in 0..basin.len() {
                        let new_neighbors : Vec<Coord> = basin[i].get_neighbors(width, height).iter()
                            .filter(|coord| rows[coord.y as usize][coord.x as usize] != 9)
                            .filter(|coord| basin.iter().all(|other| coord.x != other.x || coord.y != other.y))
                            .map(|coord| coord.clone())
                            .collect();
                        basin.extend(new_neighbors);
                    }
                }
                basins.push(basin.len());
            }
        }
    }
    basins.sort_by(|a, b| b.cmp(a));
    let answer = (1..3).fold(basins[0], |a, b| basins[b] * a);
    println!("{}", answer);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
