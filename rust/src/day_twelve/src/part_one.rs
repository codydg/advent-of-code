use std::env;
use std::fs;
use std::collections::HashMap;

struct Path {
    path : Vec<String>
}

impl Path {
    fn new(start : String) -> Path {
        Path{path: vec!(start)}
    }

    fn last(&self) -> &String {
        self.path.last().unwrap()
    }

    fn contains(&self, elem : &String) -> bool {
        self.path.iter().any(|s| s.eq(elem))
    }

    fn add(&mut self, new : String) {
        self.path.push(new);
    }

    fn not_clone(&self) -> Path { // TODO: Figure out why I can't implement clone
        Path{path:self.path.clone()}
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let start_str = "start".to_string();
    let end_str = "end".to_string();

    let mut adjacency : HashMap<String, Vec<String>> = HashMap::new();
    adjacency.insert(end_str.clone(), vec![]);

    contents.trim_end() // Trim file
        .split("\n") // Split lines
        .map(|x| x.trim_end()) // Trim each line
        .map(|x| x.split("-") // Split each line into its two parts
            .collect::<Vec<_>>())
        .filter(|x| x.len() == 2) // Only grab lines that are two parts
        .for_each(|x| {
            if x[0].ne(&start_str) && x[1].ne(&end_str) {
                insert(&mut adjacency, x[1], x[0]);
            }
            if x[1].ne(&start_str) && x[0].ne(&end_str) {
                insert(&mut adjacency, x[0], x[1]);
            }
        });

    let mut paths : Vec<Path> = vec!(Path::new("start".to_string()));
    let mut idx = 0usize;
    while idx != paths.len() {
        let path = paths.get(idx).unwrap().not_clone();
        let path_end = path.last();
        if path_end.ne("end") {
            adjacency.get(path_end)
                .unwrap()
                .iter()
                .filter(|next| next.chars().any(|c| c.is_uppercase()) || !path.contains(next))
                .for_each(|next| {
                    let mut new_path = path.not_clone();
                    new_path.add(next.clone());
                    paths.push(new_path);
                });
        }
        idx += 1;
    }
    println!("{}",
        paths.iter()
            .filter(|path| path.last().eq("end"))
            .count());
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn insert(adjacency : &mut HashMap<String, Vec<String>>, k : &str, v : &str) {
    if adjacency.contains_key(k) {
        adjacency.get_mut(k).unwrap().push(v.to_string());
    } else {
        adjacency.insert(k.to_string(), vec!(v.to_string()));
    }
}
