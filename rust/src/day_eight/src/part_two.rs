use std::env;
use std::fs;
use std::collections::HashSet;
use regex::Regex;
use lazy_static::lazy_static;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    lazy_static! {
        static ref INPUT_REGEX: Regex = Regex::new("([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*\\|\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)").unwrap();
    }

    let num_segments : Vec<HashSet<char>> = [ "abcefg", "cf", "acdeg", "acdfg", "bcdf", "abdfg", "abdefg", "acf", "abcdefg", "abcdfg" ].iter().map(|x| x.chars().collect()).collect();
    let mut final_output : usize = 0;
    for line in contents.trim_end().split("\n").map(|x| INPUT_REGEX.captures_iter(x)) {
        for capture in line {
            let inputs_strs : Vec<&str> = (1..=10).map(|idx| &capture[idx]).collect();
            let outputs_strs : Vec<&str> = (11..=14).map(|idx| &capture[idx]).collect();

            let inputs = make_nums(inputs_strs, true);
            let outputs = make_nums(outputs_strs, false);

            let mut solution_map : [char; 7] = [ ' ', ' ', ' ', ' ', ' ', ' ', ' ' ];

            // Solve for 'a': The value in position 1 that's not in position 0
            let mut set_a = inputs[1].clone();
            set_a.retain(|x| !inputs[0].contains(x));
            assert_eq!(set_a.len(), 1);
            for a in set_a {
                solution_map[a as usize] = 'a';
            }

            // Solve for 'd' and 'g': The values in positions 3, 4, AND 5 are a, d, g.
            // a is solved, d is in position 2, and g is not.
            let mut set_1 = inputs[3].clone();
            set_1.retain(|x| inputs[4].contains(x) && inputs[5].contains(x) && solution_map[*x as usize] == ' ');
            assert_eq!(set_1.len(), 2);
            for d_or_g in set_1 {
                if inputs[2].contains(&d_or_g) {
                    solution_map[d_or_g as usize] = 'd';
                } else {
                    solution_map[d_or_g as usize] = 'g';
                }
            }

            // Solve for 'b' and 'e': The values in positions 3, 4, OR 5 that are not solved, and are not in position 0 are b and e.
            // b is in position 2, and e is not.
            let mut set_2 = inputs[3].clone();
            set_2.extend(&inputs[4]);
            set_2.extend(&inputs[5]);
            set_2.retain(|x| solution_map[*x as usize] == ' ' && !inputs[0].contains(x));
            assert_eq!(set_2.len(), 2);
            for b_or_e in set_2 {
                if inputs[2].contains(&b_or_e) {
                    solution_map[b_or_e as usize] = 'b';
                } else {
                    solution_map[b_or_e as usize] = 'e';
                }
            }

            let mut set_f = inputs[6].clone();
            set_f.retain(|x| inputs[7].contains(x) && inputs[8].contains(x) && solution_map[*x as usize] == ' ');
            assert_eq!(set_f.len(), 1);
            for f in set_f {
                solution_map[f as usize] = 'f';
            }

            let mut set_c = inputs[9].clone();
            set_c.retain(|x| solution_map[*x as usize] == ' ');
            assert_eq!(set_c.len(), 1);
            for c in set_c {
                solution_map[c as usize] = 'c';
            }

            let output_set : Vec<HashSet<char>> = outputs.iter().map(|x| x.iter().map(|x| solution_map[*x as usize]).collect()).collect();
            let output_digits : Vec<usize> = output_set.iter().map(|x| num_segments.iter().position(|y| x == y).unwrap()).collect();
            let output : usize = output_digits.iter().fold(0, |out, &val| out * 10 + val);

            final_output += output;
        }
    }
    println!("{}", final_output);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn make_nums(strs : Vec<&str>, sort : bool) -> Vec<HashSet<u8>> {
    let mut nums : Vec<HashSet<u8>> = strs.iter().map(|input| input.chars().map(|x| x as u8 - 'a' as u8).collect()).collect::<Vec<_>>();
    if sort {
        nums.sort_by(|a, b| a.len().cmp(&b.len()));
    }
    nums
}
