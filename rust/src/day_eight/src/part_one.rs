use std::env;
use std::fs;
use regex::Regex;
use lazy_static::lazy_static;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    lazy_static! {
        static ref INPUT_REGEX: Regex = Regex::new("([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*\\|\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)\\s*([a-g]+)").unwrap();
    }

    let num_segments = [ 6, 2, 5, 5, 4, 5, 6, 3, 7, 6 ];

    let mut total_count = 0;
    for line in contents.trim_end().split("\n").map(|x| INPUT_REGEX.captures_iter(x)) {
        for capture in line {
            let outputs : Vec<&str> = (11..=14).map(|idx| &capture[idx]).collect();

            for output in outputs {
                let output_len = output.len();
                let mut output_count = 0;
                for num_segment in num_segments {
                    if output_len == num_segment {
                        output_count += 1;
                    }
                }
                if output_count == 1 {
                    total_count += 1;
                }
            }
        }
    }

    println!("{}", total_count)
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
