use regex::Regex;
use std::cmp;
use std::env;
use std::fmt;
use std::fs;

type Num = i32;

#[derive(Clone, PartialEq)]
struct Point {
    x: Num,
    y: Num,
}

#[derive(Clone)]
struct Vent {
    p1: Point,
    p2: Point,
}

#[derive(PartialEq)]
enum VentType {
    Horizontal,
    Vertical,
    Diagonal,
}

impl Vent {
    fn get_type(&self) -> VentType {
        if self.p1.y == self.p2.y {
            return VentType::Horizontal;
        } else if self.p1.x == self.p2.x {
            return VentType::Vertical;
        }
        VentType::Diagonal
    }
}

impl fmt::Display for Vent {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} -> {}", self.p1, self.p2)
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let lines = contents.split("\n").collect::<Vec<_>>();
    let mut vents: Vec<Vent> = Vec::new();
    let mut min_point = Point {
        x: Num::MAX,
        y: Num::MAX,
    };
    let mut max_point = Point {
        x: Num::MIN,
        y: Num::MIN,
    };
    for full_line in lines {
        let line = full_line.trim_end();
        if line.len() == 0 {
            continue;
        }

        lazy_static::lazy_static! {
            static ref LINE_REGEX: Regex = Regex::new("([0-9]+),([0-9]+)\\s*->\\s*([0-9]+),([0-9]+)").unwrap();
        }
        for capture in LINE_REGEX.captures_iter(line) {
            let vent = Vent {
                p1: Point {
                    x: get_num(&capture[1]),
                    y: get_num(&capture[2]),
                },
                p2: Point {
                    x: get_num(&capture[3]),
                    y: get_num(&capture[4]),
                },
            };
            vents.push(vent.clone());
            min_point = Point {
                x: cmp::min(cmp::min(vent.p1.x, vent.p2.x), min_point.x),
                y: cmp::min(cmp::min(vent.p1.y, vent.p2.y), min_point.y),
            };
            max_point = Point {
                x: cmp::max(cmp::max(vent.p1.x, vent.p2.x), max_point.x),
                y: cmp::max(cmp::max(vent.p1.y, vent.p2.y), max_point.y),
            };
        }
    }
    let width = max_point.x - min_point.x + 1;
    let height = max_point.y - min_point.y + 1;
    let mut map = vec![vec![0 as Num; height as usize]; width as usize];
    for vent in vents {
        if vent.get_type() == VentType::Diagonal {
            continue;
        }

        let mut loc = vent.p1.clone();
        inc_map(&mut map, &loc, &min_point);
        while loc != vent.p2 {
            if loc.x > vent.p2.x {
                loc.x -= 1;
            } else if loc.x < vent.p2.x {
                loc.x += 1;
            }

            if loc.y > vent.p2.y {
                loc.y -= 1;
            } else if loc.y < vent.p2.y {
                loc.y += 1;
            }
            inc_map(&mut map, &loc, &min_point);
        }
    }

    let mut count = 0;
    for col in &map {
        for elem in col {
            if *elem > 1 {
                count += 1;
            }
        }
    }

    println!("{}", count);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_num(num_str: &str) -> Num {
    match num_str.parse::<Num>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}

fn inc_map(map : &mut Vec<Vec<Num>>, loc : &Point, min_point : &Point) {
    map[(loc.x - min_point.x) as usize][(loc.y - min_point.y) as usize] += 1;
}
