use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let mut counts : Vec<i32> = Vec::new();
    let mut first_run = true;
    for line in contents.split("\n") {
        let short_line = line.trim_end();
        if short_line.len() == 0 {
            continue;
        }

        match first_run {
            true => counts = vec![0i32; short_line.len()],
            false => assert_eq!(counts.len(), short_line.len())
        }
        first_run = false;

        for (i, digit) in short_line.chars().enumerate() {
            counts[i] += match digit {
                '1' => 1i32,
                '0' => -1i32,
                _ => {
                    panic!("Unknown character {}", digit)
                }
            }
        }
    }

    let mut gamma : u32 = 0;
    let mut epsilon : u32 = 1;
    for count in counts {
        gamma *= 2;
        if count > 0 {
            gamma += 1;
        }
        epsilon *= 2;
    }
    epsilon -= gamma + 1;
    let power = epsilon * gamma;
    println!("{}", power);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
