use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let zero = 48u8;
    let one = 49u8;

    let lines = contents.split("\n").collect::<Vec<_>>();

    let mut mut_lines = lines.clone();
    mut_lines.retain(|&x| x.len() > 0);
    let mut i = 0;
    while mut_lines.len() > 1 {
        let counts = get_counts(&mut_lines);
        let pass = if counts[i] < 0 {zero} else {one};
        mut_lines.retain(|&x| x.as_bytes()[i] == pass);
        i += 1;
    }
    let oxygen = bin_to_num(mut_lines[0]);

    mut_lines = lines.clone();
    mut_lines.retain(|&x| x.len() > 0);
    i = 0;
    while mut_lines.len() > 1 {
        let counts = get_counts(&mut_lines);
        let pass = if counts[i] >= 0 {zero} else {one};
        mut_lines.retain(|&x| x.as_bytes()[i] == pass);
        i += 1;
    }
    let co2 = bin_to_num(mut_lines[0]);

    let life_support = oxygen * co2;
    println!("{}", life_support);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_counts(lines : &Vec<&str>) -> Vec<i32> {
    let mut counts : Vec<i32> = Vec::new();
    let mut first_run = true;
    for line in lines {
        let short_line = line.trim_end();
        if short_line.len() == 0 {
            continue;
        }

        match first_run {
            true => counts = vec![0i32; short_line.len()],
            false => assert_eq!(counts.len(), short_line.len())
        }
        first_run = false;

        for (i, digit) in short_line.chars().enumerate() {
            counts[i] += match digit {
                '1' => 1i32,
                '0' => -1i32,
                _ => {
                    panic!("Unknown character {}", digit)
                }
            }
        }
    }

    counts
}

fn bin_to_num(num_str : &str) -> u64 {
    let mut num = 0u64;
    for num_char in num_str.trim_end().chars() {
        num *= 2;
        match num_char {
            '1' => num += 1,
            '0' => {},
            _ => {
                panic!("Unrecognized result!")
            }
        }
    }
    num
}
