use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let lines = contents.trim_end().split("\n").map(|x| x.trim_end()).collect::<Vec<_>>();
    let mut scores : Vec<u64> = lines.iter().map(|x| get_score(x)).filter(|&x| x != 0).collect();
    scores.sort();

    println!("{}", scores[(scores.len() - 1) / 2]);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_score(line : &str) -> u64 {
    let mut unmatched : Vec<char> = Vec::new();
    for letter in line.chars() {
        match letter {
            '{' | '[' | '(' | '<' => unmatched.push(letter),
            '}' => {
                match unmatched.last() {
                    None => return 0,
                    Some('{') => {
                        unmatched.pop();
                    },
                    Some(_) => return 0
                }
            },
            ']' => {
                match unmatched.last() {
                    None => return 0,
                    Some('[') => {
                        unmatched.pop();
                    },
                    Some(_) => return 0
                }
            },
            ')' => {
                match unmatched.last() {
                    None => return 0,
                    Some('(') => {
                        unmatched.pop();
                    },
                    Some(_) => return 0
                }
            },
            '>' => {
                match unmatched.last() {
                    None => return 0,
                    Some('<') => {
                        unmatched.pop();
                    },
                    Some(_) => return 0
                }
            },
            _ => panic!("Unknown bracket: {}", letter)
        };
    }
    unmatched.iter().rev().fold(0u64, |score, &letter| {
        score * 5 + match letter {
            '(' => 1,
            '[' => 2,
            '{' => 3,
            '<' => 4,
            _ => panic!("Unknown opening bracket: {}", letter)
        }
    })
}
