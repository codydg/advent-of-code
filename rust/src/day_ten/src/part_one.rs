use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let lines = contents.trim_end().split("\n").map(|x| x.trim_end()).collect::<Vec<_>>();
    let score = lines.iter().fold(0, |a, &b| a + get_score(b));
    println!("{}", score);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_score(line : &str) -> u32 {
    let mut unmatched : Vec<char> = Vec::new();
    for letter in line.chars() {
        match letter {
            '{' | '[' | '(' | '<' => unmatched.push(letter),
            '}' => {
                match unmatched.last() {
                    None => return 1197,
                    Some('{') => {
                        unmatched.pop();
                    },
                    Some(_) => return 1197
                }
            },
            ']' => {
                match unmatched.last() {
                    None => return 57,
                    Some('[') => {
                        unmatched.pop();
                    },
                    Some(_) => return 57
                }
            },
            ')' => {
                match unmatched.last() {
                    None => return 3,
                    Some('(') => {
                        unmatched.pop();
                    },
                    Some(_) => return 3
                }
            },
            '>' => {
                match unmatched.last() {
                    None => return 25137,
                    Some('<') => {
                        unmatched.pop();
                    },
                    Some(_) => return 25137
                }
            },
            _ => panic!("Unknown letter: {}", letter)
        };
    }
    0
}
