use std::env;
use std::fs;
use regex::Regex;
use lazy_static::lazy_static;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    lazy_static! {
        static ref DOT_REGEX: Regex = Regex::new("([0-9]+),([0-9]+)").unwrap();
        static ref FOLD_REGEX: Regex = Regex::new("fold along ([x|y])=([0-9]+)").unwrap();
    }

    let lines = contents.trim_end().split("\n").map(|x| x.trim_end()).collect::<Vec<_>>();

    let coords : Vec<(usize, usize)> = lines.iter()
        .map(|line| DOT_REGEX.captures_iter(line).collect::<Vec<_>>())
        .filter(|caps| caps.len() == 1)
        .map(|caps| (get_num(&caps[0][1]), get_num(&caps[0][2])))
        .collect();

    let folds : Vec<(char, usize)> = lines.iter()
        .map(|line| FOLD_REGEX.captures_iter(line).collect::<Vec<_>>())
        .filter(|caps| caps.len() == 1)
        .map(|caps| (caps[0][1].to_string(), get_num(&caps[0][2])))
        .map(|caps| (caps.0.chars().next().unwrap(), caps.1))
        .collect();

    let max_x = folds.iter()
        .filter(|fold| fold.0 == 'x')
        .map(|fold| fold.1)
        .max()
        .unwrap() * 2;

    let max_y = folds.iter()
        .filter(|fold| fold.0 == 'y')
        .map(|fold| fold.1)
        .max()
        .unwrap() * 2;

    let mut map : Vec<Vec<bool>> = (0..=max_y)
        .map(|_| (0..=max_x)
            .map(|_| false)
            .collect())
        .collect();

    for coord in coords {
        map[coord.1][coord.0] = true;
    }

    for fold in folds {
        match fold.0 {
            'x' => fold_x(&mut map, fold.1),
            'y' => fold_y(&mut map, fold.1),
            other => panic!("Unknown fold direction: {}", other)
        }
    }

    for row in map {
        println!("{}", row.iter()
            .map(|b| if *b {'#'} else {'.'})
            .collect::<String>());
    }
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_num(num_str: &str) -> usize {
    match num_str.parse::<usize>() {
        Ok(num) => num,
        Err(error) => {
            panic!("Error while retrieving num: {:?}", error);
        }
    }
}

fn fold_x(map : &mut Vec<Vec<bool>>, loc : usize) {
    let width = map[0].len() - 1; // Ignoring the fold line makes indexing easier
    let expected_loc = width / 2;
    if loc != expected_loc {
        panic!("Bad fold location: {} | Width: {} | Expected: {}", loc, width, expected_loc);
    }
    for row in map {
        for i in 0..loc {
            row[i] = row[i] || row[width - i];
        }
        row.truncate(loc);
    }
}

fn fold_y(map : &mut Vec<Vec<bool>>, loc : usize) {
    let height = map.len() - 1; // Ignoring the fold line makes indexing easier
    let expected_loc = height / 2;
    if loc != expected_loc {
        panic!("Bad fold location: {} | Height: {} | Expected: {}", loc, height, expected_loc);
    }
    for y in 0..loc {
        for x in 0..map[y].len() {
            map[y][x] = map[y][x] || map[height - y][x];
        }
    }
    map.truncate(loc);
}
