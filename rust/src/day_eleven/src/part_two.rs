use std::env;
use std::fs;

struct Field {
    octopi : Vec<Vec<u32>>,
    width : usize,
    height : usize
}

impl Field {
    fn new(octopi : Vec<Vec<u32>>) -> Field {
        let width = octopi[0].len();
        let height = octopi.len();
        Field{octopi:octopi, width:width, height:height}
    }

    fn check_flash(&mut self, x : usize, y : usize) -> usize {
        self.octopi[y][x] += 1;
        let mut flashes = 0usize;
        if self.octopi[y][x] == 10 {
            flashes += 1;
            for x_delta in 0..=2 {
                if (x == 0 && x_delta == 0) || (x == self.width - 1 && x_delta == 2) {
                    continue;
                }
                for y_delta in 0..=2 {
                    if (y == 0 && y_delta == 0) || (y == self.height - 1 && y_delta == 2) || (x_delta == 1 && y_delta == 1) {
                        continue;
                    }
                    flashes += self.check_flash(x + x_delta - 1, y + y_delta - 1);
                }
            }
        }
        flashes
    }

    fn clamp(&mut self) {
        for element in self.octopi.iter_mut().flat_map(|r| r.iter_mut()).filter(|x| **x > 9) {
            *element = 0;
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let mut field = Field::new(contents.trim_end().split("\n").map(|x| x.trim_end().chars().map(|x| x.to_digit(10).unwrap()).collect()).collect());

    let mut steps = 0;
    loop {
        steps += 1;
        let mut flashes = 0usize;
        for x in 0..field.width {
            for y in 0..field.height {
                flashes += field.check_flash(x, y);
            }
        }
        field.clamp();

        if flashes == field.width * field.height {
            break;
        }
    }
    println!("{}", steps);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}
