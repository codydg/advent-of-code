use std::env;
use std::fs;
use std::cmp;
use cmp::Ordering;

#[derive(Clone, PartialEq, Eq)]
struct Node {
    dist: u32,
    cost: u32,
    x: isize,
    y: isize
}

impl Node {
    fn new(x: isize, y: isize, cost: u32) -> Node {
        Node{ x: x, y: y, cost: cost, dist: u32::MAX }
    }

    fn is(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl Ord for Node {
    fn cmp(&self, other: &Self) -> Ordering {
        self.dist.cmp(&other.dist)
    }
}

impl PartialOrd for Node {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = parse_config(&args);

    let contents = fs::read_to_string(filename).expect("Something went wrong reading the file");

    let costs = contents.trim_end() // Trim file
        .split("\n") // Split lines
        .map(|x| x.trim_end()) // Trim each line
        .map(|x| x.chars() // Split each line into its chars
            .map(|x| x.to_digit(10u32).unwrap())
            .collect::<Vec<_>>())
        .collect::<Vec<_>>();

    let (mut nodes, goal_node) = {
        let mut nodes : Vec<Node> = Vec::new();
        let mut y = 0;
        for row in costs {
            let mut x = 0;
            for cost in row {
                let mut node = Node::new(x, y, cost);
                if node.x == 0 && node.y == 0 {
                    node.dist = 0;
                }
                nodes.push(node);
                x += 1;
            }
            y += 1;
        }
        let goal_node = nodes.get(nodes.len() - 1).unwrap().clone();
        (nodes, goal_node)
    };

    let finished_node = get_finished_node(&mut nodes, goal_node);
    println!("{}", finished_node.dist);
}

fn parse_config(args: &[String]) -> &str {
    let filename = &args[1];

    filename
}

fn get_finished_node(nodes : &mut Vec<Node>, goal_node : Node) -> Node {
    loop {
        let node = nodes.iter().min().unwrap().clone();
        if node.is(&goal_node) {
            return node.clone();
        }

        nodes.iter_mut()
            .filter(|n| (n.x - node.x).abs() + (n.y - node.y).abs() == 1)
            .for_each(|n| {
                n.dist = cmp::min(node.dist + n.cost, n.dist);
            });

        nodes.remove(nodes.iter().position(|n| n.eq(&node)).unwrap());
    }
}
