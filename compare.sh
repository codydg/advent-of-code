run_case() {
    day=$1
    part=$2
    file=$3

    rustOut=$(./rust.sh $day $part $file 2> /dev/null)
    pythonOut=$(./python.sh $day $part $file 2> /dev/null)
    javaOut=$(./java.sh $day $part $file 2> /dev/null)
    if [ "$rustOut" = "$pythonOut" ] && [ "$rustOut" = "$javaOut" ]
    then
        echo -e "\t\tFile "$file" has an agreed answer of "$rustOut
    else
        echo -e "\t\tFile "$file" has different answers!"
        echo -e "\t\t\tRust:   "$rustOut
        echo -e "\t\t\tPython: "$pythonOut
        echo -e "\t\t\tJava:   "$javaOut
    fi
}

days=$(comm -12 <(comm -12 <(ls java) <(ls python)) <(ls rust/src) | grep -v example | sed 's/day_//')
parts='one two'
files='sample input'

if [ $# -ge 3 ]
then
    files=$3
fi

if [ $# -ge 2 ]
then
    parts=$2
fi

if [ $# -ge 1 ]
then
    days=$1
fi

echo
for day in $days
do
    echo "Day "$day
    for part in $parts
    do
        echo -e "\tPart "$part
        for file in $files
        do
            run_case $day $part $file
        done
    done
done
