import argparse


def increment(coord, field):
    field[coord[1]][coord[0]] += 1
    return field[coord[1]][coord[0]]


def get_neighbors(coord):
    return [
        (coord[0] - 1, coord[1] - 1),
        (coord[0] - 1, coord[1]),
        (coord[0] - 1, coord[1] + 1),
        (coord[0], coord[1] - 1),
        (coord[0], coord[1] + 1),
        (coord[0] + 1, coord[1] - 1),
        (coord[0] + 1, coord[1]),
        (coord[0] + 1, coord[1] + 1)]


def pulse(coord, field, width, height):
    if increment(coord, field) == 10:
        return sum([pulse(coord, field, width, height) for coord in get_neighbors(coord) if \
            coord[0] >= 0 and coord[1] >= 0 and coord[0] < width and coord[1] < height]) + 1
    else:
        return 0


def main(input_file):
    with open(input_file, 'r') as input_file:
        field = [[int(letter) for letter in line] for line in input_file.read().splitlines()]

    width = len(field[0])
    height = len(field)

    pulses = 0
    for step in range(100):
        for x in range(width):
            for y in range(height):
                pulses += pulse((x, y), field, width, height)
        field = [[elem if elem < 10 else 0 for elem in row] for row in field]
    print(pulses)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
