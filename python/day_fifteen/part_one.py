import argparse


class Node:
    def __init__(self, x, y, cost):
        self.x = x
        self.y = y
        self.cost = cost
        self.dist = float('inf')


def main(input_file):
    with open(input_file, 'r') as input_file:
        nodes = [[Node(x, y, int(digit)) for x, digit in enumerate(row)] for y, row in enumerate(input_file.read().split())]

    nodes[0][0].dist = 0
    goal = nodes[-1][-1]

    deltas = [(-1, 0), (0, -1), (1, 0), (0, 1)]
    available = [[True for elem in row] for row in nodes]

    def key(n):
        return n.dist

    def min_node():
        node_list = []
        for row in nodes:
            for elem in row:
                if elem.dist is not None and available[elem.y][elem.x]:
                    node_list.append(elem)
        return min(node_list, key=key)

    node = min_node()
    while node != goal:
        neighbors = [(node.y + delta[1], node.x + delta[0]) for delta in deltas]
        for y, x in neighbors:
            if 0 <= x <= goal.x and 0 <= y <= goal.y:
                neighbor = nodes[y][x]
                neighbor.dist = min(neighbor.dist, neighbor.cost + node.dist)
        available[node.y][node.x] = False
        node = min_node()

    print(node.dist)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
