import argparse
import re


def test_bingo(board):
    marked_board = [[elem is None for elem in row] for row in board]
    return any([all(marked_row) for marked_row in marked_board])\
        or any([all([row[i] for row in marked_board]) for i in range(5)])


def score(board):
    return sum([sum([elem for elem in row if elem is not None]) for row in board])


def process_calls(boards, calls):
    has_bingo = [False] * len(boards)

    for call in calls:
        boards = [[[None if elem == call else elem for elem in row] for row in board] for board in boards]
        new_bingo = [not already_has_bingo and test_bingo(board) for already_has_bingo, board in zip(has_bingo, boards)]
        has_bingo = [a or b for a, b in zip(has_bingo, new_bingo)]
        for i, bingo in enumerate(new_bingo):
            if bingo:
                return score(boards[i]) * call


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    calls = [int(elem) for elem in lines.pop(0).split(',')]

    reg = re.compile(r'\s*([0-9]+)\s*' * 5)
    groups = [[int(elem) for elem in reg.match(line).groups()] for line in lines if len(line) != 0]
    n_boards = len(groups) // 5
    assert(n_boards * 5 == len(groups))

    boards = [groups[i * 5:(i + 1) * 5] for i in range(n_boards)]

    print(process_calls(boards, calls))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
