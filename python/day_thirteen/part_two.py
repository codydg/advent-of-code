import argparse
from os import error


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    x_instr = 'fold along x'
    y_instr = 'fold along y'
    coords = [[int(spl) for spl in line] for line in [line.split(',') for line in lines if ',' in line]]
    folds = [(line[0], int(line[1])) for line in [line.split('=') for line in lines if '=' in line]]

    max_x = max([fold[1] for fold in folds if fold[0] == x_instr]) * 2 + 1
    max_y = max([fold[1] for fold in folds if fold[0] == y_instr]) * 2 + 1

    field = [[False for x in range(max_x)] for y in range(max_y)]
    for coord in coords:
        field[coord[1]][coord[0]] = True

    for fold in folds:
        if fold[0] == x_instr:
            width = len(field[0]) - 1
            assert fold[1] == width / 2
            field = [[row[x] or row[width - x] for x in range(fold[1])] for row in field]
        elif fold[0] == y_instr:
            height = len(field) - 1
            assert fold[1] == height / 2
            field = [[field[y][x] or field[height - y][x] for x in range(len(field[y]))] for y in range(fold[1])]
        else:
            raise AssertionError('Unknown instruction: ' + fold[0])
    
    print('\n'.join([''.join(['#' if elem else '.' for elem in row]) for row in field]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
