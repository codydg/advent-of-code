import argparse
from math import prod


class Packet:
    def __init__(self, text, binary, layers=None, layer=0):
        if layers is None:
            layers = []
        self.text = text
        self.binary = binary
        self.layers = layers
        self.layer = layer
        self.strings = ['', '', '']
        self.index = 0
        self.sub_packets = []
        self.bits_since_write = []

        # Version
        self.version = Packet.bits_to_num(self.get_bits(3))
        self.write('v' + str(self.version))

        # Type
        packet_type = Packet.bits_to_num(self.get_bits(3))
        self.write('t' + str(packet_type))

        if packet_type == 4:
            another_val = True
            value_bits = []
            while another_val:
                bitset = self.get_bits(5)
                another_val = bitset[0]
                value_bits += bitset[1:]
            self.value = Packet.bits_to_num(value_bits)
            self.write(str(self.value))
        else:
            length_id = self.get_bit()
            if length_id:
                num_packets = Packet.bits_to_num(self.get_bits(11))
                self.write(f'{num_packets} packets')
                while num_packets > 0:
                    self.get_sub_packet()
                    num_packets -= 1
            else:
                num_bits = Packet.bits_to_num(self.get_bits(15))
                self.write(f'{num_bits} bits')
                while num_bits > 0:
                    packet = self.get_sub_packet()
                    num_bits -= packet.index
            sub_values = [packet.value for packet in self.sub_packets]
            if packet_type == 0:
                self.value = sum(sub_values)
            elif packet_type == 1:
                self.value = prod(sub_values)
            elif packet_type == 2:
                self.value = min(sub_values)
            elif packet_type == 3:
                self.value = max(sub_values)
            else:
                assert len(sub_values) == 2
                if packet_type == 5:
                    self.value = 1 if sub_values[0] > sub_values[1] else 0
                elif packet_type == 6:
                    self.value = 1 if sub_values[0] < sub_values[1] else 0
                elif packet_type == 7:
                    self.value = 1 if sub_values[0] == sub_values[1] else 0
                else:
                    print("ERROR READING PACKETS")
                    self.value = None

    def write(self, text):
        write_length = max(len(text), len(self.bits_since_write)) + 1
        text = text.ljust(write_length)
        bit_text = ''.join(self.bits_since_write).ljust(write_length)
        self.bits_since_write = []

        self.layers += [self.layer] * write_length
        self.strings[0] += bit_text
        self.strings[1] += '|' + ('-' * (write_length - 3)) + '| '
        self.strings[2] += text

    def get_sub_packet(self):
        packet = Packet(self.text[self.index:], self.binary[self.index:], self.layers, self.layer + 1)
        self.sub_packets.append(packet)
        self.index += packet.index

        for i in range(len(self.strings)):
            self.strings[i] += packet.strings[i]

        return packet

    def print(self):
        print('\n'.join(self.strings))

    def get_bits(self, count):
        return [self.get_bit() for i in range(count)]

    def get_bit(self):
        bit = self.binary[self.index]
        text = self.text[self.index]
        self.index += 1
        self.bits_since_write += text
        return bit

    def sum_version(self):
        return self.version + sum([packet.sum_version() for packet in self.sub_packets])

    @staticmethod
    def bits_to_num(bits):
        num = 0
        for bit in bits:
            num <<= 1
            num += 1 if bit else 0
        return num


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = [bin(int(line, 16))[2:].zfill(len(line) * 4) for line in input_file.read().splitlines()]

    for line in lines:
        packet = Packet(line, [num == '1' for num in line])
        # packet.print()
        print(packet.value)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
