import argparse


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    polymer = {}
    for comb in [lines[0][idx:idx+2] for idx in range(len(lines[0]) - 1)]:
        polymer.setdefault(comb, 0)
        polymer[comb] += 1

    replacements = {}
    for line in lines[2:]:
        split = line.split(' -> ')
        replacements[split[0]] = (split[0][0] + split[1], split[1] + split[0][1])

    for day in range(10):
        poly_copy = polymer.copy()
        for comb, count in polymer.items():
            if comb in replacements:
                poly_copy[comb] -= count
                for rep in replacements[comb]:
                    poly_copy.setdefault(rep, 0)
                    poly_copy[rep] += count
        polymer = {comb: count for comb, count in poly_copy.items() if count != 0}

    counts = {}
    for comb, count in polymer.items():
        for c in comb:
            counts.setdefault(c, 0)
            counts[c] += count
    minmax = [(v + 1) // 2 for v in [min(counts.values()), max(counts.values())]]
    print(minmax[1] - minmax[0])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
