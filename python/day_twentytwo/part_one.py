import argparse
import re


class Instruction:
    def __init__(self, groups):
        assert(len(groups) == 7)
        if groups[0] == 'on':
            self.state = True
        else:
            assert(groups[0] == 'off')
            self.state = False
        
        nums = [int(v) for v in groups[1:]]
        self.x = nums[0:2]
        self.y = nums[2:4]
        self.z = nums[4:6]

    def __str__(self):
        return ('on' if self.state else 'off') + f': ({self.x[0]}, {self.x[1]}), ({self.y[0]}, {self.y[1]}), ({self.z[0]}, {self.z[1]})'


def main(input_file):
    roi = ((-50, 50), (-50, 50), (-50, 50))
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    reg = re.compile(r'([onf]+) x=(-?[0-9]+)..(-?[0-9]+),y=(-?[0-9]+)..(-?[0-9]+),z=(-?[0-9]+)..(-?[0-9]+)')

    instructions = [Instruction(reg.match(line).groups()) for line in lines if len(line) != 0]
    on_cubes = set()
    for instruction in instructions:
        f = on_cubes.add if instruction.state else on_cubes.discard
        for x in range(max(instruction.x[0], roi[0][0]), min(instruction.x[1], roi[0][1]) + 1):
            for y in range(max(instruction.y[0], roi[1][0]), min(instruction.y[1], roi[1][1]) + 1):
                for z in range(max(instruction.z[0], roi[2][0]), min(instruction.z[1], roi[2][1]) + 1):
                    f((x, y, z))
    print(len(on_cubes))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
