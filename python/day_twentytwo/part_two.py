import argparse
import re


idxs = [(0, 1), (2, 3), (4, 5)]


class Region:
    def __init__(self, cuboid=None):
        self._cuboids = []
        if cuboid is not None:
            self._cuboids.append(cuboid)
    
    def apply(self, instruction):
        if instruction.state:
            to_add = Region(cuboid=instruction.cuboid)
            for c in self._cuboids:
                to_add.subtract(c)
            self._cuboids.extend(to_add._cuboids)
        else:
            self.subtract(instruction.cuboid)
    
    def subtract(self, other):
        new_cuboids = []
        for cuboid in self._cuboids:
            overlaps = True
            for li, hi in idxs:
                if cuboid[hi] < other[li] or cuboid[li] > other[hi]:
                    overlaps = False
                    break
            if overlaps:
                mut = cuboid.copy()
                for li, hi in idxs:
                    if mut[hi] > other[hi]:
                        new_cuboids.append([(other[hi] + 1) if li == i else v for i, v in enumerate(mut)])
                        mut[hi] = other[hi]
                    if mut[li] < other[li]:
                        new_cuboids.append([(other[li] - 1) if hi == i else v for i, v in enumerate(mut)])
                        mut[li] = other[li]
            else:
                new_cuboids.append(cuboid)
        self._cuboids = new_cuboids

    def count(self):
        return sum((c[1] - c[0] + 1) * (c[3] - c[2] + 1) * (c[5] - c[4] + 1) for c in self._cuboids)


class Instruction:
    def __init__(self, groups):
        assert(len(groups) == 7)
        if groups[0] == 'on':
            self.state = True
        else:
            assert(groups[0] == 'off')
            self.state = False
        
        self.cuboid = [int(v) for v in groups[1:]]

    def __str__(self):
        return ('on' if self.state else 'off') + f': ({self.cuboid[0]}, {self.cuboid[1]}), ({self.cuboid[2]}, {self.cuboid[3]}), ({self.cuboid[4]}, {self.cuboid[5]})'


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    reg = re.compile(r'([onf]+) x=(-?[0-9]+)..(-?[0-9]+),y=(-?[0-9]+)..(-?[0-9]+),z=(-?[0-9]+)..(-?[0-9]+)')

    instructions = [Instruction(reg.match(line).groups()) for line in lines if len(line) != 0]
    r = Region()
    for instruction in instructions:
        r.apply(instruction)
    print(r.count())

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
