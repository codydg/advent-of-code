import argparse
import re


def add_point(counts, point, min_x, min_y):
    counts[point[0] - min_x][point[1] - min_y] += 1


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    reg = re.compile(r'([0-9]+),([0-9]+)\s*->\s*([0-9]+),([0-9]+)')
    lines = [[int(elem) for elem in reg.match(line).groups()] for line in lines if len(line) != 0]
    min_x = min([min(group[0], group[2]) for group in lines])
    min_y = min([min(group[1], group[3]) for group in lines])
    width = max([max(group[0], group[2]) for group in lines]) - min_x + 1
    height = max([max(group[1], group[3]) for group in lines]) - min_y + 1
    counts = [[0] * height for _ in range(width)]
    for line in lines:
        add_point(counts, line[0:2], min_x, min_y)
        while line[0] != line[2] or line[1] != line[3]:
            if line[0] < line[2]:
                line[0] += 1
            elif line[0] > line[2]:
                line[0] -= 1
            if line[1] < line[3]:
                line[1] += 1
            elif line[1] > line[3]:
                line[1] -= 1
            add_point(counts, line[0:2], min_x, min_y)

    print(sum([sum([elem > 1 for elem in row]) for row in counts]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
