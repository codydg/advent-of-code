import argparse


def to_bin(line):
    num = 0
    for let in line:
        num *= 2
        if let == '#':
            num += 1
    return num


def pad(image, pad_char):
    image = [(pad_char*2) + line + (pad_char*2) for line in image]
    append_str = pad_char * len(image[0])
    image.insert(0, append_str)
    image.insert(0, append_str)
    image.append(append_str)
    image.append(append_str)
    return image


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    algorithm = lines[0]
    image = [line for line in lines[1:] if len(line) > 0]

    pad_char = '.'
    for _ in range(2):
        image = pad(image, pad_char)

        new_image = []
        for y in range(len(image) - 2):
            rows = image[y:y+3]
            new_row = ''
            for x in range(len(rows[1]) - 2):
                new_row += algorithm[to_bin(''.join([row[x:x+3] for row in rows]))]
            new_image.append(new_row)

        image = new_image
        if pad_char == '.':
            pad_char = algorithm[0]
        else:
            pad_char = algorithm[-1]

    print(sum([len([elem for elem in row if elem == '#']) for row in image]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
