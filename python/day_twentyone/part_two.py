import argparse
import re
from collections import defaultdict


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    reg = re.compile(r'Player [0-9]+ starting position: ([0-9]+)')
    positions = [int(reg.match(line).groups()[0]) for line in lines if len(line) != 0]
    n_players = len(positions)
    assert(n_players == 2)

    n_positions = 10
    final_score = 21
    outcomes = {3:1, 4:3, 5:6, 6:7, 7:6, 8:3, 9:1}

    # Player: Location -> Score -> Count
    create_player = lambda: defaultdict(lambda: defaultdict(lambda: 0))
    players = [create_player() for _ in range(n_players)]
    for position, player in zip(positions, players):
        player[position][0] = 1

    wins = [0] * n_players

    player_id = -1
    while len(players[player_id]) != 0:
        player_id = (player_id + 1) % n_players
        n_alt_games = sum(map(lambda sc: sum(sc.values()), players[(player_id + 1) % n_players].values()))
        old_player = players[player_id]
        new_player = create_player()

        for location, score_count in old_player.items():
            for score, count in score_count.items():
                for outcome, quantity in outcomes.items():
                    new_location = (location + outcome - 1) % n_positions + 1
                    new_score = score + new_location
                    new_count = count * quantity
                    if new_score >= final_score:
                        wins[player_id] += new_count * n_alt_games
                    else:
                        new_player[new_location][new_score] += new_count

        players[player_id] = new_player
    
    print(max(wins))

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
