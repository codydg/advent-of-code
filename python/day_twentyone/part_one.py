import argparse
import re


class Die:
    def __init__(self, sides):
        self._sides = sides
        self._roll = 0
        self._num_rolls = 0
    
    def roll(self):
        self._roll += 1
        self._num_rolls += 1
        if self._roll > self._sides:
            self._roll = 1
        return self._roll
    
    @property
    def num_rolls(self):
        return self._num_rolls


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    reg = re.compile(r'Player [0-9]+ starting position: ([0-9]+)')
    positions = [int(reg.match(line).groups()[0]) for line in lines if len(line) != 0]
    n_players = len(positions)

    n_positions = 10
    n_die_rolls = 3
    final_score = 1000
    scores = [0] * n_players
    player = -1
    die = Die(100)

    while scores[player] < final_score:
        player = (player + 1) % n_players
        positions[player] = (positions[player] + sum([die.roll() for i in range(n_die_rolls)]) - 1) % n_positions + 1
        scores[player] += positions[player]

    player = (player + 1) % n_players
    print(scores[player] * die.num_rolls)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
