from functools import reduce
import argparse


def get_intersection(inputs):
    return reduce(lambda s1, s2: s1.intersection(s2), inputs[1:], inputs[0])


def get_union(inputs):
    return reduce(lambda s1, s2: s1.union(s2), inputs[1:], inputs[0])


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = [[word.upper() for word in line.split(' ') if word != '|'] for line in input_file.read().splitlines()]

    assert all([len(line) == 14 for line in lines])

    to_count = [2, 3, 4, 7]
    output = sum([sum([len(word) in to_count for word in line[10:]]) for line in lines])

    print(output)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
