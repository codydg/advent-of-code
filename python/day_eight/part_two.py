from functools import reduce
import argparse


def get_intersection(inputs):
    return reduce(lambda s1, s2: s1.intersection(s2), inputs[1:], inputs[0])


def get_union(inputs):
    return reduce(lambda s1, s2: s1.union(s2), inputs[1:], inputs[0])


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = [[word.upper() for word in line.split(' ') if word != '|'] for line in input_file.read().splitlines()]

    assert all([len(line) == 14 for line in lines])

    vals = [set(val) for val in ['abcefg', 'cf', 'acdeg', 'acdfg', 'bcdf', 'abdfg', 'abdefg', 'acf', 'abcdefg', 'abcdfg']]
    output = 0
    for line in lines:
        lookup = {}
        inputs = sorted([set(word) for word in line[:10]], key=len)
        outputs = [set(word) for word in line[10:]]
        c2 = inputs[0]
        c3 = inputs[1]
        c4 = inputs[2]
        c5 = inputs[3:6]
        c6 = inputs[6:9]
        c7 = inputs[9]
        a = c3 - c2
        assert len(a) == 1
        lookup[a.pop()] = 'a'
        set_1 = get_intersection(c5) - set(lookup.keys())
        assert len(set_1) == 2
        d, g = set_1.pop(), set_1.pop()
        if d in c4:
            assert g not in c4
        else:
            d, g = g, d
            assert d in c4
        lookup[d] = 'd'
        lookup[g] = 'g'
        set_2 = get_union(c5) - set(lookup.keys()) - c2
        assert len(set_2) == 2
        b, e = set_2.pop(), set_2.pop()
        if b in c4:
            assert e not in c4
        else:
            b, e = e, b
            assert b in c4
        lookup[b] = 'b'
        lookup[e] = 'e'
        f = get_intersection(c6) - set(lookup.keys())
        assert len(f) == 1
        lookup[f.pop()] = 'f'
        c = c7 - set(lookup.keys())
        assert len(c) == 1
        lookup[c.pop()] = 'c'
        val = reduce(lambda x, y: x * 10 + y, [vals.index({lookup[i] for i in output}) for output in outputs], 0)
        output = output + val

    print(output)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
