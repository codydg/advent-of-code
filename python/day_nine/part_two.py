import argparse
from math import prod


def get_val(coord, field):
    return field[coord[1]][coord[0]]


def get_neighbors(x, y, width, height):
    return[coord for coord in [(x + 1, y), (x - 1, y), (x, y - 1), (x, y + 1)]
           if 0 <= coord[0] < width and 0 <= coord[1] < height]


def is_basin(x, y, field, width, height):
    neighbors = get_neighbors(x, y, width, height)
    base_height = get_val((x, y), field)
    return all([get_val(neighbor, field) > base_height for neighbor in neighbors])


def get_basin_size(basin, field, width, height):
    coordinates = [basin]
    for coord in coordinates:
        for neighbor in get_neighbors(coord[0], coord[1], width, height):
            if get_val(neighbor, field) != 9 and neighbor not in coordinates:
                coordinates.append(neighbor)
    return len(coordinates)


def main(input_file):
    with open(input_file, 'r') as input_file:
        field = [[int(letter) for letter in line] for line in input_file.read().splitlines()]

    width = len(field[0])
    height = len(field)
    basins = sum([[(x, y) for y in range(height) if is_basin(x, y, field, width, height)] for x in range(width)], [])
    basin_sizes = [get_basin_size(basin, field, width, height) for basin in basins]
    basin_sizes.sort()

    print(prod(basin_sizes[-3:]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
