import argparse


def get_val(coord, field):
    return field[coord[1]][coord[0]]


def is_basin(x, y, field, height, width):
    neighbors = [coord for coord in [(x+1, y), (x-1, y), (x, y-1), (x, y+1)] if
                 0 <= coord[0] < width and 0 <= coord[1] < height]
    base_height = get_val((x, y), field)
    return all([get_val(neighbor, field) > base_height for neighbor in neighbors])


def main(input_file):
    with open(input_file, 'r') as input_file:
        field = [[int(letter) for letter in line] for line in input_file.read().splitlines()]

    width = len(field[0])
    height = len(field)
    basins = sum([[(x, y) for y in range(height) if is_basin(x, y, field, height, width)] for x in range(width)], [])

    print(sum([get_val(basin, field) + 1 for basin in basins]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
