import argparse


class Path:
    def __init__(self, path, visited_twice=False):
        self.path = path
        self._visited_twice = visited_twice

    def last(self):
        return self.path[-1]

    def add(self, elem):
        visited_twice = self._visited_twice
        if elem.islower():
            if self._visited_twice:
                if elem in self.path:
                    return None
            else:
                if elem in self.path:
                    visited_twice = True
        p = self.path.copy()
        p.append(elem)
        return Path(p, visited_twice)


def main(input_file):
    adjacency_map = {'end': []}

    def add_adjacency(adj):
        if adj[0] != 'end' and adj[1] != 'start':
            adjacency_map.setdefault(adj[0], []).append(adj[1])
        if adj[0] != 'start' and adj[1] != 'end':
            adjacency_map.setdefault(adj[1], []).append(adj[0])

    with open(input_file, 'r') as input_file:
        for line in input_file.read().split():
            add_adjacency(line.split('-'))

    paths = [Path(['start'])]
    for path in paths:
        last = path.last()
        if last == 'end':
            continue
        for neighbor in adjacency_map[last]:
            new_path = path.add(neighbor)
            if new_path is not None:
                paths.append(new_path)

    print(len([p for p in paths if p.last() == 'end']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
