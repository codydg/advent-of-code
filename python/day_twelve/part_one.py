import argparse


def main(input_file):
    adjacency_map = {'end': []}

    def add_adjacency(adj):
        adjacency_map.setdefault(adj[0], []).append(adj[1])
        adjacency_map.setdefault(adj[1], []).append(adj[0])

    with open(input_file, 'r') as input_file:
        for line in input_file.read().split():
            add_adjacency(line.split('-'))

    paths = [['start']]
    for path in paths:
        last = path[-1]
        if last == 'end':
            continue
        for neighbor in adjacency_map[last]:
            if neighbor.islower() and neighbor in path:
                continue
            new_path = path.copy()
            new_path.append(neighbor)
            if new_path not in paths:
                paths.append(new_path)

    print(len([p for p in paths if p[-1] == 'end']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
