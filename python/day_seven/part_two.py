import argparse


def get_fuel(crabs, position):
    distances = [abs(crab - position) for crab in crabs]
    return sum([(distance + 1) * distance // 2 for distance in distances])


def main(input_file):
    with open(input_file, 'r') as input_file:
        nums_str = [line for line in input_file.read().splitlines() if len(line) > 0]

    assert (len(nums_str) == 1)
    crabs = [int(num) for num in nums_str[0].split(',')]
    crabs.sort()
    fuel = min([get_fuel(crabs, position) for position in range(crabs[0], crabs[-1] + 1)])
    print(fuel)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
