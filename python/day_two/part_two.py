import argparse
import re


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    reg = re.compile(r'^([a-zA-Z]*)\s*([0-9]*)$')
    matches = [reg.match(line).groups() for line in lines]

    x = 0
    y = 0
    aim = 0
    for match in matches:
        dir = match[0]
        amount = int(match[1])
        if dir == 'forward':
            x += amount
            y += amount * aim
        elif dir == 'down':
            aim += amount
        elif dir == 'up':
            aim -= amount
    print(x * y)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
