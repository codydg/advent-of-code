import argparse


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    gamma = 0
    epsilon = 1
    for bit in [sum([line[i] == '1' for line in lines]) > len(lines) / 2 for i in range(len(lines[0]))]:
        epsilon *= 2
        gamma = gamma * 2 + bit
    epsilon = epsilon - gamma - 1

    print(gamma * epsilon)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
