import argparse


def get_bias(lines, i):
    return sum([line[i] == '1' for line in lines]) - len(lines) / 2


def bin_to_dec(line):
    dec = 0
    for elem in line:
        dec = dec * 2 + (elem == '1')
    return dec


def get_param(lines, crit):
    i = 0
    while len(lines) > 1:
        to_keep = '1' if crit(get_bias(lines, i)) else '0'
        lines = [line for line in lines if line[i] == to_keep]
        i += 1
    return bin_to_dec(lines[0])


def get_oxygen(lines):
    return get_param(lines, lambda bias: bias >= 0)


def get_co2(lines):
    return get_param(lines, lambda bias: bias < 0)


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    oxygen = get_oxygen(lines)
    co2 = get_co2(lines)

    print(oxygen * co2)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
