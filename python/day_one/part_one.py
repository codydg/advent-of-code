import argparse


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    depths = [int(line) for line in lines]
    print(sum([depths[i + 1] - depths[i] > 0 for i in range(len(depths) - 1)]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
