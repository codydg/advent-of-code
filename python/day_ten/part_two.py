import argparse

letter_scores = {'}': 1197, '>': 25137, ']': 57, ')': 3, '{': 3, '<': 4, '[': 2, '(': 1}


def score_incomplete(line):
    score = 0
    for bracket in line[::-1]:
        score *= 5
        score += letter_scores[bracket]
    return score


def process(line):
    opened = ''
    for bracket in line:
        if bracket in '{[(<':
            opened += bracket
        elif bracket in '}])>':
            if len(opened) == 0:
                return letter_scores[bracket]
            opening = opened[-1]
            opened = opened[:-1]
            if (bracket == '}' and opening != '{') \
                    or (bracket == ']' and opening != '[') \
                    or (bracket == ')' and opening != '(') \
                    or (bracket == '>' and opening != '<'):
                return letter_scores[bracket]
        else:
            return None
    return opened


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    processed_lines = [process(line) for line in lines]
    scores = [score for score in processed_lines if type(score) is int]
    incomplete_scores = [score_incomplete(line) for line in processed_lines if type(line) is str]
    incomplete_scores.sort()

    print(incomplete_scores[len(incomplete_scores) // 2])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
