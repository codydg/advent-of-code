import argparse

letter_scores = {'}': 1197, '>': 25137, ']': 57, ')': 3}


def get_score(line):
    opened = ''
    for bracket in line:
        if bracket in '{[(<':
            opened += bracket
        elif bracket in '}])>':
            if len(opened) == 0:
                return letter_scores[bracket]
            opening = opened[-1]
            opened = opened[:-1]
            if (bracket == '}' and opening != '{') \
                    or (bracket == ']' and opening != '[') \
                    or (bracket == ')' and opening != '(') \
                    or (bracket == '>' and opening != '<'):
                return letter_scores[bracket]
        else:
            return None
    return 0


def main(input_file):
    with open(input_file, 'r') as input_file:
        lines = input_file.read().splitlines()

    scores = [get_score(line) for line in lines]

    print(sum(scores))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
