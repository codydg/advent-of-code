import argparse
import re


def main(input_file):
    reg = re.compile(r'target area: x=([-0-9]+)\.\.([-0-9]+),\s*y=([-0-9]+)\.\.([-0-9]+)')
    with open(input_file, 'r') as input_file:
        targets = [[int(val) for val in reg.match(line).groups()] for line in input_file.read().splitlines()]

    for target in targets:
        # Assume target is below shooter since it is in all examples
        print(sum(range(-target[2])))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
