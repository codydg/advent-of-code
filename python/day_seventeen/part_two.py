import argparse
import re


def simulate(x_vel, y_vel, target):
    x = 0
    y = 0
    while y > target[2]:
        x += x_vel
        y += y_vel
        if x_vel > 0:
            x_vel -= 1
        y_vel -= 1
        if target[0] <= x <= target[1] and target[2] <= y <= target[3]:
            return True
    return False


def main(input_file):
    reg = re.compile(r'target area: x=([-0-9]+)\.\.([-0-9]+),\s*y=([-0-9]+)\.\.([-0-9]+)')
    with open(input_file, 'r') as input_file:
        targets = [[int(val) for val in reg.match(line).groups()] for line in input_file.read().splitlines()]

    for target in targets:
        distinct_velocities = 0
        max_x_vel = target[1]
        # Assume target is below shooter to the right since it is in all examples
        for y_vel in range(target[2], -target[2]): # Intentionally leaving out -target[2]
            for x_vel in range(1, max_x_vel + 1):
                if simulate(x_vel, y_vel, target):
                    distinct_velocities += 1
        print(distinct_velocities)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
