import argparse


class SnailfishNumber:
    def __init__(self, left, right):
        self.left = self.make(left)
        self.right = self.make(right)

    def make(self, opt):
        if isinstance(opt, list):
            return SnailfishNumber(opt[0], opt[1])
        else:
            return opt

    def add(self, other):
        return SnailfishNumber(self, other).reduce()

    def reduce(self):
        while self.reduce_step():
            pass
        return self

    def reduce_step(self):
        return self.check_explosion(0) is not None or self.check_split()

    def check_explosion(self, layer):
        if layer == 4:
            return [self.left, self.right, True]

        if isinstance(self.left, SnailfishNumber):
            explosion = self.left.check_explosion(layer + 1)
            if explosion is not None:
                if explosion[2]:
                    self.left = 0
                    explosion[2] = False
                if explosion[1] is not None:
                    if isinstance(self.right, SnailfishNumber):
                        self.right.add_left(explosion[1])
                    else:
                        self.right += explosion[1]
                    explosion[1] = None
                return explosion

        if isinstance(self.right, SnailfishNumber):
            explosion = self.right.check_explosion(layer + 1)
            if explosion is not None:
                if explosion[2]:
                    self.right = 0
                    explosion[2] = False
                if explosion[0] is not None:
                    if isinstance(self.left, SnailfishNumber):
                        self.left.add_right(explosion[0])
                    else:
                        self.left += explosion[0]
                    explosion[0] = None
                return explosion

        return None

    def check_split(self):
        if isinstance(self.left, SnailfishNumber):
            if self.left.check_split():
                return True
        elif self.left >= 10:
            self.left = SnailfishNumber(self.left // 2, (self.left + 1) // 2)
            return True

        if isinstance(self.right, SnailfishNumber):
            if self.right.check_split():
                return True
        elif self.right >= 10:
            self.right = SnailfishNumber(self.right // 2, (self.right + 1) // 2)
            return True

        return False

    def add_left(self, value):
        if isinstance(self.left, SnailfishNumber):
            self.left.add_left(value)
        else:
            self.left += value

    def add_right(self, value):
        if isinstance(self.right, SnailfishNumber):
            self.right.add_right(value)
        else:
            self.right += value

    def get_magnitude(self):
        mag = 0
        if isinstance(self.left, SnailfishNumber):
            mag += 3 * self.left.get_magnitude()
        else:
            mag += 3 * self.left

        if isinstance(self.right, SnailfishNumber):
            mag += 2 * self.right.get_magnitude()
        else:
            mag += 2 * self.right

        return mag

    def __str__(self):
        return f'[{self.left},{self.right}]'


def main(input_file):
    with open(input_file, 'r') as input_file:
        sfs = [eval(line) for line in input_file.read().splitlines()]

    for sf in sfs:
        assert(len(sf) == 2)
    sfs = [SnailfishNumber(sf[0], sf[1]) for sf in sfs]

    solution = None
    for sf in sfs:
        if solution is None:
            solution = sf
        else:
            solution = solution.add(sf)

    print(solution.get_magnitude())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    args = parser.parse_args()
    main(args.input_file)
