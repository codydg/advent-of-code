import argparse


def main(input_file, days):
    with open(input_file, 'r') as input_file:
        nums_str = [line for line in input_file.read().splitlines() if len(line) > 0]

    assert(len(nums_str) == 1)
    fish = [0] * 9
    for num_str in nums_str[0].split(','):
        fish[int(num_str)] += 1

    for day in range(days):
        rollover = fish[0]
        fish[0:-1] = fish[1:]
        fish[-1] = rollover
        fish[6] += rollover

    print(sum(fish))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_file', type=str, help='File to read as an input')
    parser.add_argument('--days', type=int, help='Number of days to simulate', required=False, default=80)
    args = parser.parse_args()
    main(args.input_file, args.days)
