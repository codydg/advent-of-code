cd python
source $(find venv -name activate)
python 'day_'$1'/part_'$2'.py' '../data/day_'$1'/'$3'.txt'
deactivate
