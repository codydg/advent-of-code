import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class part_one {
    private static class Node {
        int dist = Integer.MAX_VALUE;
        int travelCost;
        int x;
        int y;

        Node(int x, int y, int travelCost) {
            this.x = x;
            this.y = y;
            this.travelCost = travelCost;
        }

        @Override
        public String toString() {
            return "(" + x + ", " + y + "), Cost: " + travelCost + ", Dist: " + (dist == Integer.MAX_VALUE ? "Unknown" : dist);
        }
    }

    public static void main(String[] args) throws IOException {
        PriorityQueue<Node> nodes = new PriorityQueue<>((a, b) -> Integer.compare(a.dist, b.dist));

        Node node = null;
        int y = 0;
        for (var line : Files.lines(Paths.get(args[0])).toList()) {
            int x = 0;
            for (int num : line
                .chars()
                .map(Character::getNumericValue)
                .boxed()
                .toList()) {
                node = new Node(x, y, num);
                if (x == 0 && y == 0) {
                    node.dist = 0;
                }
                nodes.add(node);
                x++;
            }
            y++;
        }

        var goalNode = node;
        node = nodes.poll();
        while (node != goalNode) {
            List<Node> nodifierNodes = new ArrayList<>();
            for (var neighbor : nodes) {
                // Test if neighbor
                if (Math.abs(neighbor.x - node.x) + Math.abs(neighbor.y - node.y) == 1) {
                    int dist = node.dist + neighbor.travelCost;
                    if (dist < neighbor.dist) {
                        neighbor.dist = dist;
                        nodifierNodes.add(neighbor);
                    }
                }
            }
            for (var modifiedNode : nodifierNodes) {
                if (!nodes.remove(modifiedNode)) {
                    System.err.println("Could not remove node: " + modifiedNode);
                }

                nodes.add(modifiedNode);
            }
            node = nodes.poll();
        }

        System.out.println(node.dist);
    }
}
