import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;

public class part_two {
    static class State {
        int x = 0;
        int y = 0;
        int aim = 0;
    }
    public static void main(String[] args) throws IOException {
        var regex = Pattern.compile("([a-z]*)\s*([0-9]*)");
        State s = new State();

        Files.lines(Paths.get(args[0])).map(regex::matcher).filter(matcher -> matcher.find()).forEach(matcher -> {
            int amount = Integer.parseInt(matcher.group(2));
            switch (matcher.group(1)) {
                case "forward":
                    s.x += amount;
                    s.y += amount * s.aim;
                    break;
                case "up":
                    s.aim -= amount;
                    break;
                case "down":
                    s.aim +=  amount;
                    break;
                default:
                    System.err.println("Unknown command: " + matcher.group(0));
            }
        });
        System.out.println(s.x * s.y);
    }
}
