import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class part_two {
    public static void main(String[] args) throws IOException {
        List<List<Boolean>> bitsOxy = new ArrayList<>();
        for (var line : Files.lines(Paths.get(args[0])).toList()) {
            List<Boolean> bit = new ArrayList<>();
            line.chars().forEach((in) -> bit.add(in == '1'));
            bitsOxy.add(bit);
        }

        List<List<Boolean>> bitsCo2 = new ArrayList<>();
        for (var bit : bitsOxy) {
            List<Boolean> bitCopy = new ArrayList<>();
            bit.forEach(elem -> bitCopy.add(elem));
            bitsCo2.add(bitCopy);
        }

        int i = 0;
        while (bitsOxy.size() > 1) {
            final int iCopy = i;
            long count = bitsOxy.stream().filter((bit) -> bit.get(iCopy)).count();
            boolean accepted = count >= bitsOxy.size() - count;
            bitsOxy.removeIf(bit -> bit.get(iCopy) != accepted);
            i++;
        }

        i = 0;
        while (bitsCo2.size() > 1) {
            final int iCopy = i;
            long count = bitsCo2.stream().filter((bit) -> bit.get(iCopy)).count();
            boolean accepted = count < bitsCo2.size() - count;
            bitsCo2.removeIf(bit -> bit.get(iCopy) != accepted);
            i++;
        }

        System.out.println(getNum(bitsOxy.get(0)) * getNum(bitsCo2.get(0)));
    }

    private static int getNum(List<Boolean> bits) {
        int gamma = 0;
        for (var bit : bits) {
            gamma *= 2;
            if (bit) {
                gamma += 1;
            }
        }
        return gamma;
    }
}
