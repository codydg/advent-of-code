import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class part_one {
    public static void main(String[] args) throws IOException {
        List<List<Boolean>> bits = null;
        int height = 0;
        for (var line : Files.lines(Paths.get(args[0])).toList()) {
            height++;
            if (bits == null) {
                bits = new ArrayList<>();
                for (int i = 0; i < line.length(); i++) {
                    bits.add(new ArrayList<>());
                }
            }
            assert line.length() == bits.get(0).size();
            var it = bits.iterator();
            line.chars().forEach((digit) -> {
                it.next().add(digit == '1');
            });
        }

        int gamma = 0;
        int epsilon = 1;
        for (var bit : bits) {
            gamma *= 2;
            epsilon *= 2;
            if (bit.stream().filter((in) -> in).count() > height/2) {
                gamma += 1;
            }
        }
        epsilon -= gamma + 1;

        System.out.println(epsilon * gamma);
    }
}
