import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class part_one {

    private static class Path {
        private List<String> elems = new ArrayList<>();

        public Path() {}
        public Path(String...elems) {
            for (String elem : elems) {
                this.elems.add(elem);
            }
        }

        @Override
        public String toString() {
            return String.join(", ", elems);
        }

        public Path add(String elem) {
            elems.add(elem);
            return this;
        }

        public String last() {
            return elems.get(elems.size() - 1);
        }

        public Path clone() {
            Path other = new Path();
            elems.forEach(elem -> other.add(elem));
            return other;
        }

        public boolean contains(String val) {
            for (String elem : elems) {
                if (val.equals(elem)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static void main(String[] args) throws IOException {
        Map<String, List<String>> adjacency = new HashMap<>();
        adjacency.put("end", new ArrayList<>());
        Files.lines(Paths.get(args[0]))
            .map(line -> line.split("-")) // Split lines by '-' symbol
            .filter(line -> line.length == 2) // Make sure line had one '-' symbol
            .forEach(line -> {
                if (!line[0].equals("start") && !line[1].equals("end")) {
                    adjacency.computeIfAbsent(line[1], key -> new ArrayList<>()).add(line[0]);
                }
                if (!line[1].equals("start") && !line[0].equals("end")) {
                    adjacency.computeIfAbsent(line[0], key -> new ArrayList<>()).add(line[1]);
                }
            });
        List<Path> paths = new ArrayList<>();
        paths.add(new Path("start"));
        for (int i = 0; i < paths.size(); i++) {
            var path = paths.get(i);
            var last = path.last();
            if (last == "end") {
                continue;
            }
            // Update path
            for (var nextElem : adjacency.get(last)) {
                if (nextElem.equals(nextElem.toLowerCase()) && path.contains(nextElem)) {
                    continue;
                }
                paths.add(path.clone().add(nextElem));
            }
        }
        System.out.println(paths.stream()
            .filter(path -> path.last().equals("end"))
            .count());
    }
}
