import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class part_two {

    private static class Path {
        private List<String> elems = new ArrayList<>();
        private boolean hasRepeated = false;

        public Path() {}
        public Path(String...elems) {
            for (String elem : elems) {
                this.elems.add(elem);
            }
        }

        @Override
        public String toString() {
            return String.join(", ", elems);
        }

        public boolean add(String elem) {
            if (elem.equals(elem.toLowerCase()) && contains(elem)) {
                if (hasRepeated) {
                    return false;
                }
                hasRepeated = true;
            }
            elems.add(elem);
            return true;
        }

        public String last() {
            return elems.get(elems.size() - 1);
        }

        public Path clone() {
            Path other = new Path();
            elems.forEach(elem -> other.add(elem));
            other.hasRepeated = this.hasRepeated;
            return other;
        }

        public boolean contains(String val) {
            for (String elem : elems) {
                if (val.equals(elem)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static void main(String[] args) throws IOException {
        Map<String, List<String>> adjacency = new HashMap<>();
        adjacency.put("end", new ArrayList<>());
        Files.lines(Paths.get(args[0]))
            .map(line -> line.split("-")) // Split lines by '-' symbol
            .filter(line -> line.length == 2) // Make sure line had one '-' symbol
            .forEach(line -> {
                if (!line[0].equals("start") && !line[1].equals("end")) {
                    adjacency.computeIfAbsent(line[1], key -> new ArrayList<>()).add(line[0]);
                }
                if (!line[1].equals("start") && !line[0].equals("end")) {
                    adjacency.computeIfAbsent(line[0], key -> new ArrayList<>()).add(line[1]);
                }
            });
        List<Path> paths = new ArrayList<>();
        paths.add(new Path("start"));
        for (int i = 0; i < paths.size(); i++) {
            var path = paths.get(i);
            var last = path.last();
            if (last == "end") {
                continue;
            }
            // Update path
            for (var nextElem : adjacency.get(last)) {
                Path clone = path.clone();
                if (clone.add(nextElem)) {
                    paths.add(clone);
                }
            }
        }
        System.out.println(paths.stream()
            .filter(path -> path.last().equals("end"))
            .count());
    }
}
