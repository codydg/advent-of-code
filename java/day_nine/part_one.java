import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class part_one {
    private static class Coord{
        public int x;
        public int y;
        public Coord(int x, int y) {
            this.x = x;
            this.y = y;
        }
        public List<Coord> neighbors(int width, int height) {
            List<Coord> coords = new ArrayList<>();
            if (x > 0) coords.add(new Coord(x - 1, y));
            if (x < width - 1) coords.add(new Coord(x + 1, y));
            if (y > 0) coords.add(new Coord(x, y - 1));
            if (y < height - 1) coords.add(new Coord(x, y + 1));
            return coords;
        }
    }
    public static void main(String[] args) throws IOException {
        assert(args.length == 1);
        var map = Files.lines(Paths.get(args[0])).map(line -> line.chars().map(Character::getNumericValue).boxed().toList()).toList();
        int width = map.get(0).size();
        int height = map.size();

        List<Coord> lowPoints = new ArrayList<>();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Coord coord = new Coord(x, y);
                int val = map.get(y).get(x);
                if (coord.neighbors(width, height).stream().filter(o -> map.get(o.y).get(o.x) <= val).count() == 0) {
                    lowPoints.add(coord);
                }
            }
        }

        System.out.println(lowPoints.stream().mapToInt(coord -> map.get(coord.y).get(coord.x) + 1).sum());
    }
}
