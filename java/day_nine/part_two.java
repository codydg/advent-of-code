import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

public class part_two {
    private static class Coord implements Comparable<Coord>{
        public int x;
        public int y;
        public Coord(int x, int y) {
            this.x = x;
            this.y = y;
        }
        @Override
        public boolean equals(Object obj) {
            if (obj == this) return true;
            if (!(obj instanceof Coord)) return false;
            Coord c = (Coord) obj;
            return c.x == this.x && c.y == this.y;
        }
        @Override
        public int compareTo(Coord o) {
            int comparison = Integer.compare(this.x, o.x);
            if (comparison == 0) comparison = Integer.compare(this.y, o.y);
            return comparison;
        }
        public List<Coord> neighbors(int width, int height) {
            List<Coord> coords = new ArrayList<>();
            if (x > 0) coords.add(new Coord(x - 1, y));
            if (x < width - 1) coords.add(new Coord(x + 1, y));
            if (y > 0) coords.add(new Coord(x, y - 1));
            if (y < height - 1) coords.add(new Coord(x, y + 1));
            return coords;
        }
    }
    public static void main(String[] args) throws IOException {
        assert(args.length == 1);
        var map = Files.lines(Paths.get(args[0])).map(line -> line.chars().map(Character::getNumericValue).boxed().toList()).toList();
        int width = map.get(0).size();
        int height = map.size();

        List<Coord> lowPoints = new ArrayList<>();
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Coord coord = new Coord(x, y);
                int val = map.get(y).get(x);
                if (coord.neighbors(width, height).stream().filter(o -> map.get(o.y).get(o.x) <= val).count() == 0) {
                    lowPoints.add(coord);
                }
            }
        }

        System.out.println(lowPoints.stream().map(coord -> basinSize(coord, map, width, height)).sorted((a, b) -> Integer.compare(b, a)).limit(3).reduce(1, (a, b) -> a * b));
    }

    public static int basinSize(Coord lowPoint, List<List<Integer>> map, int width, int height) {
        Set<Coord> coords = new TreeSet<>();
        int size = 0;
        coords.add(lowPoint);
        while (coords.size() != size) {
            size = coords.size();
            Set<Coord> newCoords = new TreeSet<>();
            for (var coord : coords) {
                newCoords.addAll(coord.neighbors(width, height).stream().filter(c -> map.get(c.y).get(c.x) != 9).toList());
            }
            coords.addAll(newCoords);
        }
        return size;
    }
}
