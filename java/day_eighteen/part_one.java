import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Stack;

public class part_one {
    static class ExplosionResult {
        Long left;
        Long right;
        boolean setZero;
        ExplosionResult(Long left, Long right, boolean setZero) {
            this.left = left;
            this.right = right;
            this.setZero = setZero;
        }
    }
    static class SnailfishNumber {
        SnailfishNumber left = null;
        SnailfishNumber right = null;
        Long regular = null;

        SnailfishNumber(String text) {
            if (text.length() == 1) {
                regular = Long.parseLong(text);
                return;
            }
            Integer splitIdx = null;
            int open = 0;
            for (int i = 0; i < text.length(); i++) {
                char character = text.charAt(i);
                if (character == '[') {
                    open++;
                } else if (character == ']') {
                    open--;
                } else if (character == ',') {
                    if (open < 1) {
                        System.err.println(", with open == 0");
                        return;
                    }
                    if (open == 1) {
                        if (splitIdx != null) {
                            System.err.println("Multiple , with open == 1");
                            return;
                        }
                        splitIdx = i;
                    }
                }
            }
            if (splitIdx == null) {
                System.err.println("No , with open == 1");
                return;
            }
            left = new SnailfishNumber(text.substring(1, splitIdx));
            right = new SnailfishNumber(text.substring(splitIdx + 1, text.length() - 1));
        }

        SnailfishNumber(SnailfishNumber left, SnailfishNumber right) {
            this.left = left;
            this.right = right;
        }

        SnailfishNumber(Long number) {
            this.regular = number;
        }

        SnailfishNumber(Long left, Long right) {
            this.left = new SnailfishNumber(left);
            this.right = new SnailfishNumber(right);
        }

        SnailfishNumber add(SnailfishNumber other) {
            return new SnailfishNumber(this, other).reduce();
        }

        SnailfishNumber reduce() {
            while (reduceStep());
            return this;
        }

        boolean reduceStep() {
            return checkExplosion(0) != null || checkSplit();
        }

        ExplosionResult checkExplosion(int layer) {
            if (layer == 4) {
                return new ExplosionResult(left.regular, right.regular, true);
            }

            if (left.regular == null) {
                var explosion = left.checkExplosion(layer + 1);
                if (explosion != null) {
                    if (explosion.setZero) {
                        left = new SnailfishNumber(0l);
                        explosion.setZero = false;
                    }
                    if (explosion.right != null) {
                        right.addLeft(explosion.right);
                        explosion.right = null;
                    }
                    return explosion;
                }
            }

            if (right.regular == null) {
                var explosion = right.checkExplosion(layer + 1);
                if (explosion != null) {
                    if (explosion.setZero) {
                        right = new SnailfishNumber(0l);
                        explosion.setZero = false;
                    }
                    if (explosion.left != null) {
                        left.addRight(explosion.left);
                        explosion.left = null;
                    }
                    return explosion;
                }
            }

            return null;
        }

        boolean checkSplit() {
            if (regular != null) {
                return false;
            }

            if (left.checkSplit()) {
                return true;
            }
            if (left.regular != null)
            {
                long val = left.regular;
                if (val >= 10) {
                    left = new SnailfishNumber(val / 2, (val + 1) / 2);
                    return true;
                }
            }

            if (right.checkSplit()) {
                return true;
            }
            if (right.regular != null)
            {
                long val = right.regular;
                if (val >= 10) {
                    right = new SnailfishNumber(val / 2, (val + 1) / 2);
                    return true;
                }
            }

            return false;
        }

        void addLeft(long value) {
            if (regular == null) {
                left.addLeft(value);
            } else {
                regular += value;
            }
        }

        void addRight(long value) {
            if (regular == null) {
                right.addRight(value);
            } else {
                regular += value;
            }
        }

        long getMagnitude() {
            if (regular == null) {
                return 3 * left.getMagnitude() + 2 * right.getMagnitude();
            } else {
                return regular;
            }
        }
    }
    public static void main(String[] args) throws IOException {
        System.out.println(Files.lines(Paths.get(args[0]))
            .filter(line -> !line.isEmpty())
            .map(SnailfishNumber::new)
            .reduce((a, b) -> a.add(b))
            .get().getMagnitude());
    }
}
