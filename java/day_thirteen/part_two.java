import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class part_two {
    public static class Coord {
        int x;
        int y;
        Coord(MatchResult match) {
            x = Integer.parseInt(match.group(1));
            y = Integer.parseInt(match.group(2));
        }
    }
    public static class Fold {
        char dir;
        int pos;
        Fold(MatchResult match) {
            dir = match.group(1).charAt(0);
            pos = Integer.parseInt(match.group(2));
        }
    }
    public static void main(String[] args) throws IOException {
        var lines = Files.lines(Paths.get(args[0])).toList();

        var coord_regex = Pattern.compile("([0-9]+),([0-9]+)");
        var fold_regex = Pattern.compile("fold along (x|y)=([0-9]+)");

        var coords = lines.stream()
            .map(coord_regex::matcher)
            .filter(Matcher::find)
            .map(Coord::new)
            .toList();
            

        var folds = lines.stream()
            .map(fold_regex::matcher)
            .filter(Matcher::find)
            .map(Fold::new)
            .toList();

        int max_x = folds.stream()
            .filter(fold -> fold.dir == 'x')
            .mapToInt(fold -> fold.pos)
            .max()
            .getAsInt() * 2;

        int max_y = folds.stream()
            .filter(fold -> fold.dir == 'y')
            .mapToInt(fold -> fold.pos)
            .max()
            .getAsInt() * 2;

        List<List<Boolean>> field = new ArrayList<>();
        for (int y = 0; y <= max_y; y++) {
            List<Boolean> row = new ArrayList<>();
            for (int x = 0; x <= max_x; x++) {
                row.add(false);
            }
            field.add(row);
        }
        
        for (var coord : coords) {
            field.get(coord.y).set(coord.x, true);
        }

        folds.stream()
            .forEach(fold -> {
                if (fold.dir == 'x') {
                    var width = field.get(0).size() - 1;
                    if (width / 2 != fold.pos) {
                        throw new AssertionError("Bad fold location (x): " + fold.pos);
                    }
                    for (var row : field) {
                        for (int x = 0; x < fold.pos; x++) {
                            row.set(x, row.get(x) || row.get(width - x));
                        }
                        row.subList(fold.pos, row.size()).clear();
                    }
                } else if (fold.dir == 'y') {
                    var height = field.size() - 1;
                    if (height / 2 != fold.pos) {
                        throw new AssertionError("Bad fold location (y): " + fold.pos);
                    }
                    for (int y = 0; y < fold.pos; y++) {
                        var row = field.get(y);
                        var alt_row = field.get(height - y);
                        for (int x = 0; x < row.size(); x++) {
                            row.set(x, row.get(x) || alt_row.get(x));
                        }
                    }
                    field.subList(fold.pos, field.size()).clear();
                } else {
                    throw new AssertionError("Unknown fold direction: " + fold.dir);
                }
            });

        for (var row : field) {
            for (var elem : row) {
                System.out.print(elem ? '#' : '.');
            }
            System.out.println();
        }
    }
}
