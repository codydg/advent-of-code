import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class part_two {
    public static void main(String[] args) throws IOException {

        List<Set<Character>> truthData = Arrays.asList(
            Set.of('a', 'b', 'c', 'e', 'f', 'g'),
            Set.of('c', 'f'),
            Set.of('a', 'c', 'd', 'e', 'g'),
            Set.of('a', 'c', 'd', 'f', 'g'),
            Set.of('b', 'c', 'd', 'f'),
            Set.of('a', 'b', 'd', 'f', 'g'),
            Set.of('a', 'b', 'd', 'e', 'f', 'g'),
            Set.of('a', 'c', 'f'),
            Set.of('a', 'b', 'c', 'd', 'e', 'f', 'g'),
            Set.of('a', 'b', 'c', 'd', 'f', 'g')
        );
        Function<String, Stream<Set<Integer>>> getSetStream = string -> Arrays.stream(string.trim().split(" ")).map(letters -> letters.chars().boxed().collect(Collectors.toSet()));

        System.out.println(Files.lines(Paths.get(args[0]))
            .map(line -> line.split("\\|"))
            .filter(inOut -> inOut.length == 2)
            .mapToLong(inOut -> {
                var inputs = getSetStream.apply(inOut[0]).sorted((a, b) -> Integer.compare(a.size(), b.size())).toList();
                Map<Integer, Character> answerKey = new HashMap<>();
                answerKey.put(inputs.get(1).stream().filter(o -> !inputs.get(0).contains(o)).findAny().get(), 'a');

                List<Integer> dOrG = inputs.get(3).stream().filter(o -> inputs.get(4).contains(o) && inputs.get(5).contains(o) && !answerKey.keySet().contains(o)).toList();
                if (inputs.get(2).contains(dOrG.get(0))) {
                    answerKey.put(dOrG.get(0), 'd');
                    answerKey.put(dOrG.get(1), 'g');
                } else {
                    answerKey.put(dOrG.get(1), 'd');
                    answerKey.put(dOrG.get(0), 'g');
                }

                var bOrE = inputs.get(9).stream().filter(o -> !answerKey.keySet().contains(o) && !inputs.get(0).contains(o)).toList();
                if (inputs.get(2).contains(bOrE.get(0))) {
                    answerKey.put(bOrE.get(0), 'b');
                    answerKey.put(bOrE.get(1), 'e');
                } else {
                    answerKey.put(bOrE.get(1), 'b');
                    answerKey.put(bOrE.get(0), 'e');
                }

                answerKey.put(inputs.get(6).stream().filter(o -> inputs.get(7).contains(o) && inputs.get(8).contains(o) && !answerKey.keySet().contains(o)).findAny().get(), 'f');

                answerKey.put(inputs.get(9).stream().filter(o -> !answerKey.keySet().contains(o)).findAny().get(), 'c');

                var digits = getSetStream.apply(inOut[1]).map(ints -> ints.stream().map(key -> answerKey.get(key)).collect(Collectors.toSet())).map(chars -> truthData.indexOf(chars)).toList();
                long answer = 0;
                for (var digit : digits) {
                    answer = answer * 10 + digit;
                }
                return answer;
            }).sum());
    }
}
