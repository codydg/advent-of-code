import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Set;

public class part_one {
    public static void main(String[] args) throws IOException {
        final Set<Integer> toCount = Set.of(2, 3, 4, 7);

        System.out.println(Files.lines(Paths.get(args[0]))
            .map(line -> line.split("\\|"))
            .filter(v -> v.length == 2)
            .map(inOut -> inOut[1].split(" "))
            .map(Arrays::stream)
            .flatMapToInt(stream -> stream
                .mapToInt(String::length))
            .filter(toCount::contains)
            .count());
    }
}
