import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class part_two {
    public static void main(String[] args) throws IOException {
        Double lastAvg = null;
        int counter = 0;
        int i = 0;
        var depths = new int[3];
        for (var val : Files.lines(Paths.get(args[0])).map(Integer::parseInt).toList()) {
            depths[i % 3] = val;
            if (i >= 2) {
                double avgDepth = 0;
                for (var depth : depths) {
                    avgDepth += depth;
                }
                avgDepth /= 3.0;
                if (lastAvg != null && avgDepth > lastAvg) {
                    counter++;
                }
                lastAvg = avgDepth;
            }
            i++;
        }

        System.out.println(counter);
    }
}
