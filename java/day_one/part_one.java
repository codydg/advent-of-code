import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class part_one {
    public static void main(String[] args) throws IOException {
        int counter = 0;
        Integer last = null;
        for (var num : Files.lines(Paths.get(args[0])).map(Integer::parseInt).toList()) {
            if (last != null && num > last) counter++;
            last = num;
        }

        System.out.println(counter);
    }
}
