import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class part_two {
    static class Target {
        public final int minX;
        public final int maxX;
        public final int minY;
        public final int maxY;
        public Target(MatchResult match) {
            minX = Integer.parseInt(match.group(1));
            maxX = Integer.parseInt(match.group(2));
            minY = Integer.parseInt(match.group(3));
            maxY = Integer.parseInt(match.group(4));
        }
    }

    static boolean simulate(int x_vel, int y_vel, Target target) {
        int x = 0;
        int y = 0;
        while (y > target.minY) {
            x += x_vel;
            y += y_vel;
            x_vel -= Math.signum(x_vel);
            y_vel--;
            if ((target.minX <= x) && (target.maxX >= x) && (target.minY <= y) && (target.maxY >= y)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) throws IOException {
        var regex = Pattern.compile("target area: x=([-0-9]+)\\.\\.([-0-9]+),\s*y=([-0-9]+)\\.\\.([-0-9]+)");
        Files.lines(Paths.get(args[0]))
            .map(regex::matcher)
            .filter(Matcher::find)
            .map(Target::new)
            .forEach(target -> {
                // Assume target is below shooter since it is in all examples
                int distinctVelocities = 0;
                for (int y_vel = target.minY; y_vel < -target.minY; y_vel++) {
                    for (int x_vel = 1; x_vel <= target.maxX; x_vel++) {
                        if (simulate(x_vel, y_vel, target)) {
                            distinctVelocities += 1;
                        }
                    }
                }
                System.out.println(distinctVelocities);
            });
    }
}
