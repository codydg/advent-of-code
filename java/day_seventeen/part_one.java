import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class part_one {
    static class Target {
        public final int minX;
        public final int maxX;
        public final int minY;
        public final int maxY;
        public Target(MatchResult match) {
            minX = Integer.parseInt(match.group(1));
            maxX = Integer.parseInt(match.group(2));
            minY = Integer.parseInt(match.group(3));
            maxY = Integer.parseInt(match.group(4));
        }
    }

    public static void main(String[] args) throws IOException {
        var regex = Pattern.compile("target area: x=([-0-9]+)\\.\\.([-0-9]+),\s*y=([-0-9]+)\\.\\.([-0-9]+)");
        Files.lines(Paths.get(args[0]))
            .map(regex::matcher)
            .filter(Matcher::find)
            .map(Target::new)
            .forEach(target -> {
                // Assume target is below shooter since it is in all examples
                int n = -target.minY - 1;
                System.out.println(n * (n+1) / 2);
            });
    }
}
