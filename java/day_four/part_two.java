import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class part_two {
    public static class Board {
        public List<List<Integer>> values = new ArrayList<>();

        private Integer score = null;
        public Integer score(int call) {
            if (score != null) {
                return null; // Only return score once
            }

            values = values.stream().map(row -> row.stream().map(elem -> elem == null || elem == call ? null : elem).collect(Collectors.toList())).collect(Collectors.toList());

            for (int i = 0; i < 5; i++) {
                boolean rowWon = true;
                boolean colWon = true;
                for (int j = 0; j < 5; j++) {
                    rowWon &= values.get(i).get(j) == null;
                    colWon &= values.get(j).get(i) == null;
                }
                if (rowWon || colWon) {
                    score = getScore() * call;
                    return score;
                }
            }
            return null;
        }

        public int getScore() {
            return values.stream().flatMapToInt(row -> row.stream().flatMapToInt(num -> IntStream.of(num == null ? 0 : num))).sum();
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        var scanner = new Scanner(new File(args[0]));

        List<Integer> calls = getCalls(scanner.nextLine());

        List<Board> boards = new ArrayList<>();
        boards.add(new Board());
        var regex = Pattern.compile("\s*([0-9]+)\s*");
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            var matcher = regex.matcher(line);
            List<Integer> boardRow = new ArrayList<>();
            while (matcher.find()) {
                boardRow.add(Integer.parseInt(matcher.group(1)));
            }

            switch (boardRow.size()) {
                case 0:
                    continue;
                case 5:
                    Board board = boards.get(boards.size() - 1);
                    if (board.values.size() == 5) {
                        board = new Board();
                        boards.add(board);
                    }
                    board.values.add(boardRow);
                    break;
                default:
                    System.err.println("Misread line: " + line);
                    continue;
            }
        }

        int remainingWins = boards.size();
        for (var call : calls) {
            for (var board : boards) {
                var score = board.score(call);
                if (score != null && --remainingWins == 0) {
                    System.out.println(score);
                    return;
                }
            }
        }
    }

    public static List<Integer> getCalls(String line) {
        List<Integer> calls = new ArrayList<>();
        for (var call : line.split(",")) {
            calls.add(Integer.parseInt(call));
        }
        return calls;
    }
}
