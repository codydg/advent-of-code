import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class part_two {
    public static class Point implements Comparable<Point> {
        public final int x;
        public final int y;
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Point stepTo(Point other) {
            return new Point(
                x + Integer.signum(other.x - x),
                y + Integer.signum(other.y - y));
        }

        @Override
        public String toString() {
            return "(" + x + ", " + y + ")";
        }

        @Override
        public int compareTo(Point o) {
            int comparison = Integer.compare(x, o.x);
            if (comparison == 0) {
                comparison = Integer.compare(y, o.y);
            }
            return comparison;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }

            if (!(o instanceof Point)) {
                return false;
            }

            Point p = (Point) o;
            return p.x == this.x && p.y == this.y;
        }
    }

    public static class Line {
        public final Point p1;
        public final Point p2;
        public Line(MatchResult match) {
            p1 = new Point(Integer.parseInt(match.group(1)),
                           Integer.parseInt(match.group(2)));
            p2 = new Point(Integer.parseInt(match.group(3)),
                           Integer.parseInt(match.group(4)));
        }

        @Override
        public String toString() {
            return p1 + " -> " + p2;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            }

            if (!(o instanceof Line)) {
                return false;
            }

            Line l = (Line) o;
            return l.p1.equals(this.p1) && l.p2.equals(this.p2);
        }

        public List<Point> allPoints() {
            Point p = p1;
            List<Point> points = new ArrayList<>();
            points.add(p);
            while (!p.equals(p2)) {
                p = p.stepTo(p2);
                points.add(p);
            }
            return points;
        }
    }

    public static void main(String[] args) throws IOException {
        var regex = Pattern.compile("([0-9]+),([0-9]+)\s*->\s*([0-9]+),([0-9]+)");
        Map<Point, Integer> pointCounts = new TreeMap<>();
        Files.lines(Paths.get(args[0]))
            .map(regex::matcher)
            .filter(Matcher::find)
            .map(matcher -> new Line(matcher))
            .forEach(line -> line.allPoints().forEach(point -> pointCounts.put(point, pointCounts.getOrDefault(point, 0) + 1)));

        System.out.println(pointCounts.values().stream().filter(num -> num > 1).count());
    }
}
