import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class part_two {
    public static void main(String[] args) throws IOException {
        Files.lines(Paths.get(args[0])).forEach(System.out::println);
    }
}
