import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;

public class part_one {
    private static class Point {
        final int x;
        final int y;
        final int z;
        Point(int x, int y, int z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        @Override
        public String toString() {
            return x + "," + y + "," + z;
        }
    }

    private static class Scanner {
        final static List<Function<Point, Point>> rotationFunctions = Arrays.asList(
            point -> new Point(point.x, point.y, point.z),
            point -> new Point(-point.x, -point.y, point.z),
            point -> new Point(point.x, -point.y, -point.z),
            point -> new Point(-point.x, point.y, -point.z),
            point -> new Point(point.y, point.x, point.z),
            point -> new Point(-point.y, -point.x, point.z),
            point -> new Point(point.y, -point.x, -point.z),
            point -> new Point(-point.y, point.x, -point.z),
            point -> new Point(point.z, point.y, point.x),
            point -> new Point(-point.z, -point.y, point.x),
            point -> new Point(point.z, -point.y, -point.x),
            point -> new Point(-point.z, point.y, -point.x),
            point -> new Point(point.x, point.z, point.y),
            point -> new Point(-point.x, -point.z, point.y),
            point -> new Point(point.x, -point.z, -point.y),
            point -> new Point(-point.x, point.z, -point.y),
            point -> new Point(point.x, point.y, point.z),
            point -> new Point(-point.x, -point.y, point.z),
            point -> new Point(point.x, -point.y, -point.z),
            point -> new Point(-point.x, point.y, -point.z),
            point -> new Point(point.x, point.y, point.z),
            point -> new Point(-point.x, -point.y, point.z),
            point -> new Point(point.x, -point.y, -point.z),
            point -> new Point(-point.x, point.y, -point.z)
        );
        final List<Point> points;
        final String label;

        Scanner(String label) {
            points = new ArrayList<>();
            this.label = label;
        }

        Scanner(Scanner toClone, Function<Point, Point> conversion) {
            this.points = toClone.points.stream().map(conversion).toList();
            this.label = toClone.label;
        }

        void add(Point point) {
            points.add(point);
        }

        @Override
        public String toString() {
            return "--- scanner " + label + " ---\n" + String.join("\n", points.stream().map(Point::toString).toList());
        }

        List<Scanner> getRotations() {
            List<Scanner> rotations = new ArrayList<>();



            return rotations;
        }
    }

    public static void main(String[] args) throws IOException {
        List<Scanner> scanners = new ArrayList<>();
        var scannerRegex = Pattern.compile("--- scanner ([0-9])+ ---");
        var pointRegex = Pattern.compile("(.*),(.*),(.*)");
        {
            Scanner scanner = null;
            for (var line : Files.lines(Paths.get(args[0])).filter(line -> !line.isBlank()).toList()) {
                var matcher = scannerRegex.matcher(line);
                if (matcher.find()) {
                    scanner = new Scanner(matcher.group(1));
                    scanners.add(scanner);
                    continue;
                }

                matcher = pointRegex.matcher(line);
                if (matcher.find()) {
                    scanner.add(new Point(Integer.parseInt(matcher.group(1)),
                        Integer.parseInt(matcher.group(2)),
                        Integer.parseInt(matcher.group(3))));
                }
            }
        }

        System.out.println(
            String.join("\n\n", scanners.stream()
                .map(Scanner::toString)
                .toList()));
    }
}
