import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class part_one {
    public static void main(String[] args) throws IOException {
        long[] fishCount = new long[9];
        Files.lines(Paths.get(args[0]))
            .map(line -> line.split(","))
            .map(Arrays::stream)
            .forEach(line -> line
                .map(Integer::parseInt)
                .forEach(idx -> fishCount[idx]++));

        for (int day = 1; day <= 80; day++) {
            long newborns = fishCount[0];
            for (int i = 0; i < 8; i++) {
                fishCount[i] = fishCount[i + 1];
            }
            fishCount[6] += newborns;
            fishCount[8] = newborns;
        }
        System.out.println(Arrays.stream(fishCount).sum());
    }
}
