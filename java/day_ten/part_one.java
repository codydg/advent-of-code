import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Objects;
import java.util.Stack;

public class part_one {
    private static class BracketLine {
        private static final Map<Character, Character> openToClose = Map.of('[', ']', '{', '}', '(', ')', '<', '>');

        Stack<Character> open = new Stack<>();
        Character corruptedAt = null;
        BracketLine(String text) {
            for (char bracket : text.toCharArray()) {
                if (openToClose.keySet().contains(bracket)) {
                    open.add(bracket);
                } else if (open.size() == 0 || openToClose.get(open.lastElement()) != bracket) {
                    corruptedAt = bracket;
                    return;
                } else {
                    open.pop();
                }
            }
        }

        Character corrupter() {
            return corruptedAt;
        }
    }
    private static final Map<Character, Integer> scores = Map.of(']', 57, '}', 1197, ')', 3, '>', 25137);
    public static void main(String[] args) throws IOException {
        System.out.println(Files.lines(Paths.get(args[0]))
            .map(BracketLine::new)
            .map(BracketLine::corrupter)
            .filter(Objects::nonNull)
            .mapToInt(scores::get)
            .sum());
    }
}
