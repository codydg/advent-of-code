import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Stack;

public class part_two {
    private static class BracketLine {
        private static final Map<Character, Character> openToClose = Map.of('[', ']', '{', '}', '(', ')', '<', '>');
        private static final Map<Character, Integer> scores = Map.of('[', 2, '{', 3, '(', 1, '<', 4);

        Stack<Character> open = new Stack<>();
        Character corruptedAt = null;
        BracketLine(String text) {
            for (char bracket : text.toCharArray()) {
                if (openToClose.keySet().contains(bracket)) {
                    open.add(bracket);
                } else if (open.size() == 0 || openToClose.get(open.lastElement()) != bracket) {
                    corruptedAt = bracket;
                    return;
                } else {
                    open.pop();
                }
            }
        }

        Character corrupter() {
            return corruptedAt;
        }

        long score() {
            long score = 0;
            while (!open.empty()) {
                score = score * 5 + scores.get(open.pop());
            }
            return score;
        }
    }
    public static void main(String[] args) throws IOException {
        var scores = Files.lines(Paths.get(args[0]))
            .map(BracketLine::new)
            .filter(bracketLine -> bracketLine.corrupter() == null)
            .mapToLong(BracketLine::score)
            .sorted()
            .boxed()
            .toList();

        System.out.println(scores.get(scores.size() / 2));
    }
}
