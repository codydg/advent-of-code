import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;

public class part_two {
    private static class Packet {
        final Supplier<Boolean> next;
        final int version;
        final Long value;
        final List<Packet> subPackets = new ArrayList<>();
        int bitsUsed = 0;

        Packet(Iterator<Boolean> iter) {
            this(iter::next);
        }

        Packet(Supplier<Boolean> bitter) {
            // Next character obtainer should increment bit count and get the next bit.
            next = () -> {
                this.bitsUsed++;
                return bitter.get();
            };
            // Get version and type
            version = getInt(3);
            int type = getInt(3);
            if (type == 4) {
                // Type 4 is literal
                value = getLiteral();
            } else {
                if (next.get()) {
                    for (int numSubpackets = getInt(11); numSubpackets > 0; numSubpackets--) {
                        var packet = new Packet(next);
                        subPackets.add(packet);
                    }
                } else {
                    for (int numBitsLeft = getInt(15); numBitsLeft > 0;) {
                        var packet = new Packet(next);
                        numBitsLeft -= packet.bitsUsed;
                        subPackets.add(packet);
                    }
                }
                if (type > 4 && subPackets.size() != 2) {
                    System.err.println("Must have exactly 2 subpackets! Instead had " + subPackets.size());
                    value = null;
                } else {
                    switch (type) {
                        case 0:
                            value = subPackets.stream()
                                .mapToLong(Packet::getValue)
                                .sum();
                            break;
                        case 1:
                            value = subPackets.stream()
                                .mapToLong(Packet::getValue)
                                .reduce(1l, (a, b) -> a * b);
                            break;
                        case 2:
                            value = subPackets.stream()
                                .mapToLong(Packet::getValue)
                                .min()
                                .getAsLong();
                            break;
                        case 3:
                            value = subPackets.stream()
                                .mapToLong(Packet::getValue)
                                .max()
                                .getAsLong();
                            break;
                        case 5:
                            value = subPackets.get(0).value > subPackets.get(1).value ? 1l : 0l;
                            break;
                        case 6:
                            value = subPackets.get(0).value < subPackets.get(1).value ? 1l : 0l;
                            break;
                        case 7:
                            value = subPackets.get(0).value.equals(subPackets.get(1).value) ? 1l : 0l;
                            break;
                        default:
                            System.err.println("Unknown Operator Type: " + type);
                            value = null;
                    }
                }
            }
        }

        long getValue() {
            return value;
        }

        private long getLiteral() {
            boolean repeat = true;
            long literal = 0;
            while (repeat) {
                // First bit is false when this is the last set of bits
                repeat = next.get();
                // Move literal over 4 bits and read in the next 4
                literal <<= 4;
                literal += getInt(4);
            }
            return literal;
        }

        private int getInt(int bitCount) {
            int result = 0;
            while (bitCount != 0) {
                result <<= 1;
                if (next.get()) {
                    result += 1;
                }
                bitCount--;
            }
            return result;
        }

        public long sumVersion() {
            return version +
                subPackets.stream()
                    .mapToLong(Packet::sumVersion)
                    .sum();
        }
    }

    private static List<Boolean> getBits(String letters) {
        List<Boolean> bits = new ArrayList<>();
        for (int i = 0; i < letters.length(); i++) {
            // Read the hex character into an int
            int num = Character.digit(letters.charAt(i), 16);
            // Push back the 4 bits of the hex character as booleans
            for (int bit = 1 << 3; bit != 0; bit >>= 1) {
                bits.add((num & bit) != 0);
            }
        }
        return bits;
    }

    public static void main(String[] args) throws IOException {
        // Each line is treated as a separate hex packet to parse.
        // The official puzzle input is therefore one line, but the sample file has multiple lines.
        Files.lines(Paths.get(args[0])) // Read each line
            .filter(line -> !line.isEmpty()) // Filter out empty lines
            .map(part_two::getBits) // Convert string to list of bits (booleans)
            .map(List::iterator) // Get an iterator for each list of bits (one iterator per line)
            .map(Packet::new) // Convert iterator to packet
            .map(Packet::getValue) // Sum the versions of all packets and subpackets
            .forEach(System.out::println); // Print the solutions
    }
}
