import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class part_one {
    private static class Coord{
        public int x;
        public int y;
        public Coord(int x, int y) {
            this.x = x;
            this.y = y;
        }
        public List<Coord> neighbors() {
            return Arrays.asList(
                new Coord(x - 1, y - 1),
                new Coord(x - 1, y),
                new Coord(x - 1, y + 1),
                new Coord(x, y - 1),
                new Coord(x, y + 1),
                new Coord(x + 1, y - 1),
                new Coord(x + 1, y),
                new Coord(x + 1, y + 1));
        }
    }

    private static class Map{
        List<List<Integer>> map;
        int width;
        int height;
        Map(List<List<Integer>> map) {
            this.map = map;
            this.width = map.get(0).size();
            this.height = map.size();
        }
        long pulse(Coord coord) {
            var row = map.get(coord.y);
            var val = row.get(coord.x) + 1;
            row.set(coord.x, val);

            if (val == 10) {
                return coord.neighbors()
                    .stream()
                    .filter(c ->
                        c.x >= 0 &&
                        c.x < width &&
                        c.y >= 0 &&
                        c.y < height)
                    .mapToLong(this::pulse)
                    .sum() + 1;
            }
            return 0;
        }
        void cleanup() {
            map = map.stream()
                .map(row -> row.stream()
                    .map(elem -> elem >= 10 ? 0 : elem)
                    .collect(Collectors.toList()))
                    .collect(Collectors.toList());
        }
    }
    public static void main(String[] args) throws IOException {
        assert(args.length == 1);
        var map = new Map(Files.lines(Paths.get(args[0])).map(line -> line.chars().map(Character::getNumericValue).boxed().collect(Collectors.toList())).collect(Collectors.toList()));

        long pulses = 0;
        for (int i = 0; i < 100; i++) {
            for (int x = 0; x < map.width; x++) {
                for (int y = 0; y < map.height; y++) {
                    pulses += map.pulse(new Coord(x, y));
                }
            }
            map.cleanup();
        }

        System.out.println(pulses);
    }
}
