import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class part_two {
    public static void main(String[] args) throws IOException {
        var crabs = Files.lines(Paths.get(args[0]))
            .map(line -> line.split(","))
            .map(Arrays::stream)
            .flatMapToInt(line -> line.mapToInt(Integer::parseInt))
            .sorted()
            .boxed().toList();

        System.out.println(IntStream.range(crabs.get(0), crabs.get(crabs.size() - 1) + 1).map(pos -> getFuel(pos, crabs)).min().getAsInt());
    }

    private static int getFuel(int goalPos, List<Integer> crabs) {
        return crabs.stream().mapToInt(pos -> Math.abs(pos - goalPos)).map(delta -> delta * (1 + delta) / 2).sum();
    }
}
