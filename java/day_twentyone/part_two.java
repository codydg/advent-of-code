import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class part_two {
    private static final long numSpaces = 10;
    private static final long finalScore = 21;

    private static class DieOutcome {
        public final long roll;
        public final long quantity;

        public DieOutcome(Integer[] vals) {
            roll = vals[0];
            quantity = vals[1];
        }
    }

    // Outcome -> Number of ways to achieve the outcome
    private static final List<DieOutcome> dieOutcomes = Stream.of(
            new Integer[] { 3, 1 },
            new Integer[] { 4, 3 },
            new Integer[] { 5, 6 },
            new Integer[] { 6, 7 },
            new Integer[] { 7, 6 },
            new Integer[] { 8, 3 },
            new Integer[] { 9, 1 })
            .map(DieOutcome::new)
            .toList();

    private static List<Long> getWins(List<Long> startPoints) {
        Map<Long, Map<Long, Map<Long, Map<Long, Long>>>> game = new HashMap<>();
        {
            var scoreP2_Quantity = new HashMap<Long, Long>();
            scoreP2_Quantity.put(0l, 1l);

            var scoreP1_scoreP2 = new HashMap<Long, Map<Long, Long>>();
            scoreP1_scoreP2.put(0l, scoreP2_Quantity);

            var locationP2_scoreP1 = new HashMap<Long, Map<Long, Map<Long, Long>>>();
            locationP2_scoreP1.put(startPoints.get(1), scoreP1_scoreP2);

            game.put(startPoints.get(0), locationP2_scoreP1);
        }
        long p1Wins = 0;
        long p2Wins = 0;
        while (!game.isEmpty()) {
            var prevGame = game;
            game = new HashMap<>();
            for (var locationsScoresQuantity : prevGame.entrySet()) {
                var locationP1 = locationsScoresQuantity.getKey();
                for (var locationScoresQuantity : locationsScoresQuantity.getValue().entrySet()) {
                    var locationP2 = locationScoresQuantity.getKey();
                    for (var scoresQuantity : locationScoresQuantity.getValue().entrySet()) {
                        var scoreP1 = scoresQuantity.getKey();
                        for (var scoreQuantity : scoresQuantity.getValue().entrySet()) {
                            var scoreP2 = scoreQuantity.getKey();
                            var quantity = scoreQuantity.getValue();
                            for (var outcomeP1 : dieOutcomes) {
                                var p1NewLoc = (locationP1 + outcomeP1.roll) % numSpaces;
                                p1NewLoc = p1NewLoc == 0 ? numSpaces : p1NewLoc;
                                var p1NewScore = scoreP1 + p1NewLoc;

                                if (p1NewScore >= finalScore) {
                                    p1Wins += quantity * outcomeP1.quantity;
                                } else {
                                    for (var outcomeP2 : dieOutcomes) {
                                        var p2NewLoc = (locationP2 + outcomeP2.roll) % numSpaces;
                                        p2NewLoc = p2NewLoc == 0 ? numSpaces : p2NewLoc;
                                        var p2NewScore = scoreP2 + p2NewLoc;
                                        var newGameCount = outcomeP1.quantity * outcomeP2.quantity * quantity;

                                        if (p2NewScore >= finalScore) {
                                            p2Wins += newGameCount;
                                        } else {
                                            game.computeIfAbsent(p1NewLoc, l -> new HashMap<>())
                                                .computeIfAbsent(p2NewLoc, l -> new HashMap<>())
                                                .computeIfAbsent(p1NewScore, l -> new HashMap<>())
                                                .merge(p2NewScore, newGameCount, (a, b) -> a + b);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return List.of(p1Wins, p2Wins);
    }

    public static void main(String[] args) throws IOException {
        var regex = Pattern.compile("Player [0-9]+ starting position: ([0-9]+)");
        var startPoints = Files.lines(Paths.get(args[0]))
            .map(regex::matcher)
            .filter(Matcher::find)
            .map(match -> match.group(1))
            .map(Long::parseLong)
            .toList();

        assert(startPoints.size() == 2);
        var wins = getWins(startPoints);
        System.out.println(Math.max(wins.get(0), wins.get(1)));
    }
}
