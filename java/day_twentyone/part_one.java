import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class part_one {
    private static final int numSpaces = 10;
    private static final int finalScore = 1000;
    private static final int numRolls = 3;

    public static class Player {
        public int space;
        public int score = 0;

        public Player(MatchResult match) {
            space = Integer.parseInt(match.group(1));
        }
    }

    public static class Die {
        private final int sides;
        public int value = 1;
        public int numRolls = 0;

        public Die(int sides) {
            this.sides = sides;
        }

        public int roll() {
            numRolls++;
            var result = value++;
            if (value > sides) {
                value = 1;
            }
            return result;
        }
    }

    private static void playGame(List<Player> players, Die die) {
        while (true) {
            for (var player : players) {
                for (int i = 0; i < numRolls; i++) {
                    player.space += die.roll();
                }
                player.space %= numSpaces;
                if (player.space == 0) {
                    player.space = numSpaces;
                }
                player.score += player.space;
                if (player.score >= finalScore) {
                    return;
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        var regex = Pattern.compile("Player [0-9]+ starting position: ([0-9]+)");
        var players = Files.lines(Paths.get(args[0]))
            .map(regex::matcher)
            .filter(Matcher::find)
            .map(Player::new)
            .toList();

        var die = new Die(10);
        playGame(players, die);

        int loserScore = players.stream()
            .mapToInt(player -> player.score)
            .filter(score -> score < finalScore)
            .findAny()
            .getAsInt();

        System.out.println(die.numRolls * loserScore);
    }
}
