import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class part_one {
    static List<String> pad(List<String> image, char padding) {
        var padStr = Character.toString(padding);

        image = new ArrayList<>(image.stream()
            .map(line -> padStr.repeat(2) + line + padStr.repeat(2))
            .toList());

        var cap = padStr.repeat(image.get(0).length());
        image.add(0, cap);
        image.add(0, cap);
        image.add(cap);
        image.add(cap);
        return image;
    }

    public static void main(String[] args) throws IOException {
        var iter = Files.lines(Paths.get(args[0])).iterator();
        var algorithm = iter.next();
        List<String> image;
        {
            List<String> originalImage = new ArrayList<>();
            iter.forEachRemaining(line -> {
                if (!line.isBlank()) {
                    originalImage.add(line);
                }
            });
            image = originalImage;
        }

        char padChar = '.';
        for (int i = 0; i < 2; i++){
            List<String> newImage = new ArrayList<>();
            image = pad(image, padChar);
            for (int y = 1; y < image.size() - 1; y++) {
                var prevRow = image.get(y - 1);
                var row = image.get(y);
                var nextRow = image.get(y + 1);
                String newRow = "";
                for (int x = 1; x < row.length() - 1; x++) {
                    int idx = 0;
                    int fx = x;
                    for (Boolean bit : Stream.of(prevRow, row, nextRow)
                        .map(s -> s.substring(fx - 1, fx + 2))
                        .flatMapToInt(String::chars)
                        .mapToObj(c -> c == '#')
                        .toList()) {
                        idx <<= 1;
                        if (bit) {
                            idx += 1;
                        }
                    }
                    newRow += algorithm.charAt(idx);
                }
                newImage.add(newRow);
            }
            image = newImage;
            if (padChar == '.') {
                padChar = algorithm.charAt(0);
            } else {
                padChar = algorithm.charAt(511);
            }
        }

        System.out.println(
            image.stream()
                .flatMapToInt(String::chars)
                .filter(c -> c == '#')
                .count());
    }
}
