import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class part_one {
    public static void main(String[] args) throws IOException {
        var lines = Files.lines(Paths.get(args[0])).toList();
        var firstLine = lines.get(0);

        var insertions = lines.stream().map(line -> line.split(" -> "))
            .filter(line -> line.length == 2)
            .collect(Collectors.toMap(line -> line[0],
                line -> new String[]{
                    line[0].charAt(0) + line[1],
                    line[1] + line[0].charAt(1)}));

        Map<String, Integer> polymer = new HashMap<>();
        IntStream.range(0, firstLine.length() - 1)
                    .mapToObj(idx -> firstLine.charAt(idx) + String.valueOf(firstLine.charAt(idx + 1)))
                    .forEach(pair -> polymer.put(pair, polymer.getOrDefault(pair, 0) + 1));

        var finalPolymer = polymer;
        for (int day = 1; day <= 10; day++) {
            Map<String, Integer> newPoly = new HashMap<>();
            finalPolymer.forEach((key, value) -> newPoly.put(key, value));
            finalPolymer.forEach((pair, count) -> {
                var insertion = insertions.get(pair);
                if (insertion != null) {
                    newPoly.put(pair, newPoly.get(pair) - count);
                    for (var toIncrement : insertion) {
                        newPoly.put(toIncrement, newPoly.getOrDefault(toIncrement, 0) + count);
                    }
                }
            });
            finalPolymer = newPoly;
        }

        Map<Character, Integer> counts = new HashMap<>();
        finalPolymer.forEach((pair, count) -> {
            for (char letter : pair.toCharArray()) {
                counts.put(letter, counts.getOrDefault(letter, 0) + count);
            }
        });

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (var value : counts.values()) {
            min = Math.min(min, value);
            max = Math.max(max, value);
        }

        System.out.println((max + 1) / 2 - (min + 1) / 2);
    }
}
